"""
dg_elements.py
Author: Jonah Miller (jonah.maxwell.miller@gmail.com)
Time-stamp: <2016-03-17 14:51:28 (jmiller)>

This is a small library for generating 1-dimensional visualizations
given output DG methods. It contains a number of useful data
structions for this purpose.
"""

# Imports
# ======================================================================
import numpy as np # For array support
# For polynomial interpolation
import scipy.interpolate
# Numerical integration
import scipy.integrate as integrate
# Orthogonal polynomials
import numpy.polynomial as polynomial
# linear algebra
from numpy import linalg
# Python tools
from copy import copy
# Plot tools
import matplotlib as mpl
import matplotlib.pyplot as plt
# ======================================================================


# Scientific calculation choices
# ======================================================================
# Choice of interpolator
interpolator = scipy.interpolate.BarycentricInterpolator
# Choice of polynomial basis
poly = polynomial.legendre.Legendre
# ======================================================================


# Global Constants
# ======================================================================

# formulation is always discontinuous Galerkin.
FORMULATION="dg"

# Mark true for debugging statements
DEBUGGING = True

# Some names
WIDE="WIDE"
NARROW="NARROW"
AVERAGED="AVERAGED"

# How many more points the interpolator should have compared to the
# original grid
INTERP_DENSITY=10

WARNING_MESSAGE = "This is a library. You are only supposed to import it!"

# ======================================================================


# ======================================================================
# Nodal and Modal Details
# ======================================================================
def get_quadrature_points(order):
    """
    Returns the quadrature points for Gauss-Lobatto quadrature
    as a function of the order of the polynomial we want to
    represent.
    See: https://en.wikipedia.org/wiki/Gaussian_quadrature
    """
    return np.sort(np.concatenate((np.array([-1,1]),
                                   poly.basis(order).deriv().roots())))

def get_integration_weights(order):
    """
    Returns the integration weights for Gauss-Lobatto quadrature
    as a function of the order of the polynomial we want to
    represent.
    See: https://en.wikipedia.org/wiki/Gaussian_quadrature
    """
    nodes=get_quadrature_points(order)
    interior_weights = 2/((order+1)*order*poly.basis(order)(nodes[1:-1])**2)
    boundary_weights = np.array([1-0.5*np.sum(interior_weights)])
    weights = np.concatenate((boundary_weights,
                              interior_weights,
                              boundary_weights))
    return weights

def get_vandermonde_matrices(order):
    """
    Returns the Vandermonde fast-Fourier transform matrices s2c and c2s,
    which convert spectral coefficients to configuration space coefficients
    and vice-versa respectively. Requires the order of the element/method
    as input.
    """
    nodes = get_quadrature_points(order)
    s2c = np.zeros((order+1,order+1),dtype=float)
    for i in range(order+1):
        for j in range(order+1):
            s2c[i,j] = poly.basis(j)(nodes[i])
    c2s = linalg.inv(s2c)
    return s2c,c2s
    
# TODO: make these per-element parameters to support hp-refinement
QUADRATURE_POINTS = {} #get_quadrature_points(ORDER)
INTEGRATION_WEIGHTS = {} #get_integration_weights(ORDER)
S2C = {} #get_vandermonde_matrices(ORDER)
C2S = {} 
# ======================================================================


# ======================================================================
# Convenience functions for manipulating weights
# ======================================================================
def get_weights1d(order,num_elements):
    """
    Creates a set of weights for a grid with num_elements elements
    of order
    """
    elweights=get_integration_weights(order)
    boundary=elweights[0] # may want to set this to zero
    out = np.hstack([boundary]
                    +[elweights for i in range(num_elements)]
                    +[boundary])
    return out

def weight_product_3d(w1,w2,w3):
    """
    Takes three 1d vectors w1,w2,w3 and returns the equivalent array
    that operates on a 3d grid.
    """
    return (2**3)*reduce(lambda x,y: x*y,np.meshgrid(v1,v2,v3))

def get_weights3d_homogeneous(orders,numbers):
    """
    For a 3d grid, with fixed p in each direction,
    but different number of elements in each direction,
    generates the weights.

    Inputs:
            orders = 3-eleent collection.
                     orders in x,y,z directions
            numbers = 3-element collection.
                      number of elements in each direction.
    """
    assert len(orders)==3
    assert len(numbers)==3
    weights = [get_weights_1d(order,number)\
               for order,number in zip(orders,numbers)]
    return weight_product_3d(weights[0],weights[1],weights[3])

def get_weights3d_isotropic(order,num_elements):
    """
    For an isotropic 3d grid with fixed order and number of elements
    (which is the same for each direction), generate the weights.
    """
    w1d = get_weights1d(order,num_elements)
    return weight_product_3d(w1d,w1d,w1d)

# ======================================================================


# ======================================================================
# Data structures
# ======================================================================
class element1d:
    """This is one-dimensional element in the DG sense. It contains
    the order of the element, a set of positions and integration
    weights for colocation points, and the data at each colocation
    point.
    """
    def __init__(self,order,
                 colocation_points,
                 grid_values):
        """
        Constructs an element of order with the given colocation
        points and grid values. colocation points and grid values are
        assumed to be WIDE, meaning that they include hte boundary
        points for the neighboring elements.

        Also automatically calculates the interpolating polynomials
        for discontinuous and continuous interpolations
        """
        global QUADRATURE_POINTS
        global INTEGRATION_WEIGHTS
        global S2C
        global C2S
        if DEBUGGING:
            print "Points are: {}".format(colocation_points)
        if len(colocation_points) != order+3:
            print "Colocation points are wrong."
            print "N_COLOCATION = {}".format(len(colocation_points))
            print "points are:"
            print colocation_points
            raise ValueError("Wrong number of colocation points.")
        assert len(grid_values) == order + 3
        self.order = order
        try:
            self.weights = INTEGRATION_WEIGHTS[order]
        except:
            INTEGRATION_WEIGHTS[order] = get_integration_weights(order)
            self.weights = INTEGRATION_WEIGHTS[order]
        try:
            self.s2c = S2C[order]
            self.c2s = C2S[order]
        except:
            S2C[order],C2S[order] = get_vandermonde_matrices(order)
        self.x = colocation_points
        self.y = grid_values
        self.discontinuous_interpolator =\
            interpolator(self.get_x(),self.get_yNarrow())
        self.continuous_interpolator =\
            interpolator(self.get_x(),self.get_yAveraged())
        return

    def get_order(self):
        "Returns the order of approximation"
        return self.order

    def get_xWide(self):
        "Returns colocation points"
        return self.x

    def get_xNarrow(self):
        """
        Returns the colocation points for the narrow vector.
        """
        return self.get_xWide()[1:-1]

    def get_xbounds(self):
        "Returns the max and min colocation points"
        return self.x[0],self.x[-1]

    def within_domain(self,x):
        "Returns whether x is in the elements domain"
        return self.x[0] <= x <= self.x[-1]

    def get_domain(self):
        "Returns min and max values"
        return self.x[0],self.x[-1]

    def get_x(self,wide_or_narrow=NARROW):
        """
        Returns the colocation points. Defaults to narrow.
        """
        if wide_or_narrow == WIDE:
            return self.get_xWide()
        else:
            return self.get_xNarrow()

    def get_yWide(self):
        "Returns grid values"
        return self.y

    def get_yNarrow(self):
        """
        Returns the grid values for the narrow vector.
        """
        return self.get_yWide()[1:-1]

    def get_yAveraged(self):
        """
        Returns the grid values with the boundary values set to the
        average of the exterior and interior boundaries.
        """
        wide = self.get_yWide()
        left = 0.5*(wide[0]+wide[1])
        right = 0.5*(wide[-1]+wide[-2])
        narrow = copy(self.get_yNarrow())
        narrow[0] = left
        narrow[-1] = right
        return narrow

    def get_y(self,wide_narrow_averaged=NARROW):
        """
        Returns the grid values. Defaults to narrow.
        """
        if wide_narrow_averaged == WIDE:
            return self.get_yWide()
        elif wide_narrow_averaged == NARROW:
            return self.get_yNarrow()
        elif wide_narrow_averaged == AVERAGED:
            return self.get_yAveraged()
        else:
            raise ValueError("must be wide, narrow, or averaged.")

    def get_fft(self,normalized=True):
        """
        Returns the modal decomposition of a solution within an element.
        If normalized is set to true, the vector of coefficients
        is a unit vector.
        """
        modes = np.dot(self.c2s, self.get_y(NARROW))
        if normalized:
            modes /= linalg.norm(modes)
        return modes

    def get_error(self,analytic_function,x,demand_continuous=False):
        """
        Given an analytic function and a list of positions x, returns
        an array which is the analytic function minus the
        interpolating polynomial. If demand_continuous is true, the
        interpolating polynomial is generated using the AVERAGE of the
        exterior and interior faces of the elment.

        The analytic function is assumed to be a a function only of
        position.
        """
        if demand_continuous:
            interp = self.continuous_interpolator
        else:
            interp = self.discontinuous_interpolator
        return analytic_function(x) - interp(x)

    def get_squareint_error(self,analytic_function,
                            demand_continuous=False):
        """
        Given an analytic function returns the integral of the square
        of the difference between the analytic function and the grid
        values y_Narrow. Uses Gauss-Lobatto quadrature weights.

        (assumes analytic function is vectorized a la numpy.)        
        """
        square_error = \
            lambda x: (analytic_function(x) - self.__call__(x,demand_continuous))**2
        xmin,xmax = self.get_domain()
        squareint = integrate.quad(square_error,xmin,xmax,
                                  epsabs=1E-12)
        return squareint

    def get_squareint(self,
                      scale_factor=1.0,
                      demand_continuous=False):
        """
        Returns the square integral of the function in the element

        If scale_factor is selected, rescales y-values in element
        by this factor.
        """
        square = lambda x: (scale_factor*self.__call__(x,demand_continuous))**2
        xmin,xmax = self.get_domain()
        squareint = integrate.quad(square,xmin,xmax,epsabs=1E-12)
        return squareint

    def weight_sum(self):
        "The sum over all the internal weights. Useful for normalization."
        return np.sum(self.weights)

    def __len__(self):
        "Returns the order + 1"
        return self.get_order()+1

    def __call__(self,x,demand_continuous=False):
        """
        A call to the class simply calls the interpolating polynomial
        within the element. if demand_continuous is true, the interior
        and exterior element boundaries are averaged.
        """
        if demand_continuous:
            return self.continuous_interpolator(x)
        else:
            return self.discontinuous_interpolator(x)

class GlobalInterp:
    """
    This is a class for a collection of elements.
    It acts as a global interpolator. It is capable of evaluating
    at any grid point within a domain.
    """
    def __init__(self,
                 element_list):
        """
        Constructs a GlobalInterp object given a collection
        of elements.
        """
        self.elements = element_list
        self.intervals = np.empty((len(self.elements),2))
        for i,e in enumerate(self.elements):
            self.intervals[i,0],self.intervals[i,1] = e.get_xbounds()

    def xmin(self):
        return self.intervals[0,0]

    def xmax(self):
        return self.intervals[-1,1]

    def element_containing(self,x):
        """
        Finds the element or elements for which x is in the domain.
        Tries to handle edge cases correctly. Returns a list of either one
        or two elements.

        this code is not efficient.
        """
        assert self.xmin() <= x <= self.xmax()
        indexes, = np.nonzero(np.logical_and(self.intervals[...,0] <= x,
                                             x <= self.intervals[...,1]))
        return map(lambda i: self.elements[i],indexes)

    def eval_at_single(self,x):
        """
        Evaluates the interpolant at a single value x.
        """
        assert self.xmin() <= x <= self.xmax()        
        return self.element_containing(x)[0](x,True)

    def partition_domain(self,x):
        """
        Breaks a 1d array, domain, into several smaller domains,
        each of which fits in the domain of the elements in
        self.elements

        Assumes elements are all the same order
        """
        x_partitioned = np.empty(len(self.elements),dtype=np.ndarray)
        for i,interval in enumerate(self.intervals):
            truths = np.logical_and(interval[0]<=x,
                                    x<=interval[1])
            x_partitioned[i] = x[truths]
        return x_partitioned
                
        
# ======================================================================


# ======================================================================
# Data Analysis Routines
# ======================================================================
def get_elements_from_field(order,position,field):
    """
    Takes a list of positions and tensor values and an element order
    and builds a list of elements containing the relevant information.

    Assumes that an element begins at the left boundary of the array
    and that the first point in the element is double valued.

    Also assumes that there are no "ghost points" from processor
    boundaries.
    """
    elements = [element1d(order,position[i:i+order+1+2],field[i:i+order+1+2])\
                for i in range(0,len(position)-order,order+1)]
    return elements

    
def get_L2_of_unity(elements):
    "Returns the L2 norm of '1' over domain. Induces normalization factor."
    return np.sqrt(np.sum([e.weight_sum() for e in elements]))


def nicer_domain(element):
    """
    Takes an element and returns a nicer set of x values for feeding
    into the interpolating function.
    """
    x = element.get_x()
    return np.linspace(np.min(x),np.max(x),INTERP_DENSITY*len(x))


def get_domain_error(elements,analytic_function,demand_continuous=False):
    """
    Takes a list of elements and returns two numpy arrays. One is the
    domain of x values. The other contains a list,

    (analytic_function - interpolating_function)(x)

    where x is in the first array and where the interpolating function
    is extracted as defined in the documentation for the __call__
    method in element1d.
    """
    x_list = [nicer_domain(element) for element in elements]
    y_list = \
        [elements[i].get_error(analytic_function,x_list[i],demand_continuous)\
             for i in range(len(elements))]
    x = np.concatenate(x_list)
    y = np.concatenate(y_list)
    return x,y


def get_average_spectrum(elements,normalized=True):
    """
    Given a list of elements returns a the average coefficient in
    front of each mode (as a list) for the whole domain.  You can
    choose whether or not the vector of coefficients is normalized.
    """
    out = linalg.norm(np.array([element.get_fft(normalized)\
                                for element in elements]),
                      axis=0)
    return out


def get_L2_error(elements,analytic_function,
                 demand_continuous=False):
    """
    Takes a list of elements and an analytic function and returns L2
    norm of the difference between the analytic function and the
    interpolating function on an element. Uses the Gauss-Lobatto
    quadrature weights.
    """
    num =  np.sqrt(np.sum([element.get_squareint_error(analytic_function,
                                                       demand_continuous)\
                           for element in elements]))
    den = 1#get_L2_of_unity(elements)
    return num/den


def get_domain_interpolation(elements,demand_continuous=False):
    """
    Takes a list of elements and returns two numpy arrays, one
    containing a domain of x values and one containing a range of y
    values which correspond to the interpolating polynomial evaluated
    at x. If demand_continuous is true, the inteprolating polynomial
    is calculated with the inner and outer faces of the element mapped
    to their average.
    """
    x_list = [nicer_domain(element) for element in elements]
    y_list = [elements[i](x_list[i],demand_continuous) \
              for i in range(len(elements))]
    x = np.concatenate(x_list)
    y = np.concatenate(y_list)
    return x,y


def get_lifted_differences(coarse_elements,
                           fine_elements,
                           demand_continuous=False):
    """
    Takes two lists of elements, lifts the course to the fine, and
    produces a new list of elements, which are the same order and
    resolution as the fine elements but which represent

    coarse-fine

    Elements are assumed to cover the same physical domain.

    Element lists are assumed to be ordered inceasing in x.

    The fine resolution is assumed to have a number of elements equal
    to an integer or half integer multiple of the number of elements
    in the coarse resolution.
    """
    coarse_size = len(coarse_elements)
    fine_size = len(fine_elements)
    print "Ratio = {}/{}".format(fine_size,coarse_size)
    assert fine_size > coarse_size
    if fine_size % coarse_size == 0:
        print "Using integer ratio"
        print "Factor is {}".format(fine_size/coarse_size)
        return get_lifted_differences_integer(coarse_elements,
                                              fine_elements,
                                              factor=fine_size/coarse_size,
                                              demand_continuous=demand_continuous)
    elif coarse_size % 2 == 0:
        factor = int(2*(float(fine_size)/float(coarse_size)))
        print "Using half-integer ratio"
        print "Factor is {}".format(factor)
        return get_lifted_differences_half_integer(coarse_elements,
                                                   fine_elements,
                                                   factor=factor,
                                                   demand_continuous=demand_continuous)
    else:
        raise ValueError("Fine is not integer or half-integer of coarse.")



def get_lifted_differences_half_integer(coarse_elements,
                                        fine_elements,
                                        factor=3,
                                        demand_continuous=False):
    """
    Takes two lists of elements, lifts the course to the fine, and
    produces a new list of elements, which are the same order and
    resolution as the fine elements but which represent

    coarse-fine

    Elements are assumed to cover the same physical domain.

    Element lists are assumed to be ordered inceasing in x.

    The fine resolution is assumed to have a number of elements equal
    to a half integer multiple of the number of elements in the coarse
    resolution.

    Half-integer is factor/2

    This is only valid if there are an even number of fine elements
    """
    assert len(coarse_elements) % 2 == 0
    assert factor > 0
    if factor % 2 == 0:
        return get_lifted_differences_integer(coarse_elements,
                                              fine_elements,
                                              factor/2,
                                              demand_continuous)
    diff_elements = [None for element in fine_elements]
    for i in range(len(coarse_elements)/2):
        coarse_left = coarse_elements[2*i]
        coarse_right = coarse_elements[2*i+1]
        for j in range(factor):
            fine_index = factor*i+j
            fine_element = fine_elements[fine_index]
            fine_grid = fine_element.get_x(WIDE)
            differences = np.empty_like(fine_grid)
            if j < factor/2:
                for k,x in enumerate(fine_grid):
                    differences[k] = (coarse_left(x,demand_continuous)
                                      -fine_element(x,demand_continuous))
            elif j > factor/2:
                for k,x in enumerate(fine_grid):
                    differences[k] = (coarse_right(x,demand_continuous)
                                      -fine_element(x,demand_continuous))
            else:
                for k,x in enumerate(fine_grid):
                    if k <= len(fine_grid)/2:
                        differences[k] = (coarse_left(x,demand_continuous)
                                          - fine_element(x,demand_continuous))
                    else:
                        differences[k] = (coarse_right(x,demand_continuous)
                                          - fine_element(x,demand_continuous))
            diff_elements[fine_index] = element1d(fine_element.order,
                                                  fine_grid,
                                                  differences)
    return diff_elements
            

def get_lifted_differences_integer(coarse_elements,
                                   fine_elements,
                                   factor=2,
                                   demand_continuous=False):
    """
    Takes two lists of elements, lifts the course to the fine, and
    produces a new list of elements, which are the same order and
    resolution as the fine elements but which represent

    coarse-fine

    Elements are assumed to cover the same physical domain.

    Element lists are assumed to be ordered inceasing in x.

    The fine resolution is assumed to have a number of elements equal
    to an integer multiple of the number of elements in the coarse
    resolution. You can set this integer with the factor keyword.
    """
    diff_elements = [None for element in fine_elements]
    for i,coarse_element in enumerate(coarse_elements):
        for j in range(factor):
            fine_index = factor*i + j
            fine_element = fine_elements[fine_index]
            fine_grid = fine_element.get_x(WIDE)
            differences = np.empty_like(fine_grid)
            for k,x in enumerate(fine_grid):
                differences[k] = (coarse_element(x,demand_continuous)
                                  - fine_element(x,demand_continuous))
            diff_elements[fine_index] = element1d(fine_element.order,
                                                  fine_grid,
                                                  differences)
    return diff_elements
    

def get_lifted_differences_heuristic(coarse_elements,fine_elements,
                                     domain_max = 0.5,
                                     demand_continuous=False):
    """
    Takes two lists of elements, lifts the course to the fine, and
    produces a new list of elements, which are the same order and
    resolution as the fine elements but which represent

    coarse-fine

    Elements are assumed to cover the same physical domain.

    Element lists are assumed to be ordered inceasing in x.

    You must set domain_max correctly to avoid round-off error
    problems.

    This version of the code uses heuristics.
    """
    diff_elements = [None for element in fine_elements]
    coarse_index = 0
    coarse_element = coarse_elements[coarse_index]
    coarse_xmin,coarse_xmax = coarse_element.get_xbounds()
    print "coarse: {},{}".format(coarse_xmin,coarse_xmax)
    for fine_index,fine_element in enumerate(fine_elements):
        fine_grid = fine_element.get_x(WIDE)
        differences = np.empty_like(fine_grid)
        fine_xmin,fine_xmax = fine_element.get_xbounds()
        print "\tfine: {},{}".format(fine_xmin,fine_xmax)
        if (fine_xmax <= domain_max
            and not coarse_element.within_domain(fine_xmax)):
            coarse_index += 1
            coarse_element = coarse_elements[coarse_index]
            coarse_xmin,coarse_xmax = coarse_element.get_xbounds()
            print "coarse: {},{}".format(coarse_xmin,coarse_xmax)
        for i,x in enumerate(fine_grid):
            differences[i] = (coarse_element(x,demand_continuous)
                              - fine_element(x,demand_continuous))
        diff_elements[fine_index] = element1d(fine_element.order,
                                              fine_grid,
                                              differences)
    return diff_elements

    
def get_L2_norm(elements,
                scale_factor=1.0,
                demand_continuous=False,
                normalize=True):
    """
    Returns the L2 norm of all the elements
    """
    num = np.sqrt(np.sum([e.get_squareint(scale_factor,demand_continuous)\
                          for e in elements]))
    den = get_L2_of_unity(elements)
    return num/den
            

# ======================================================================


if __name__=="__main__":
    raise ImportWarning(multipatch.WARNING_MESSAGE)
