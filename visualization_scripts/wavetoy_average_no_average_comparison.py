#!/usr/bin/env python2

# Author: Jonah Miller (jonah.maxwell.miller@gmail.com)
# Time-stamp: <2016-03-29 14:48:38 (jmiller)>

# This compares pointwise error for FD and DG

# imports
import numpy as np
import matplotlib as mpl
from matplotlib import pyplot as plt
from scipy import interpolate
import h5py
import os
import dg_elements as dge

# Global constants
FONTSIZE=16
LINEWIDTH=3
ORDER=8
RHO=4
IT=60
TIME=0.75
WIDTH = 1.#2*(1./8)
XMAX = 0.5*WIDTH
XMIN = -XMAX
NUM_GHOSTS = 5
INTERP_DENSITY=10
SCALE_FACTOR=8 # multiply everything by 10^SCALE_FACTOR
NUM_SUBFIGS = 2

ELEMENT_WIDTH=1.0/RHO
ELEMENT_POSITIONS = np.arange(-0.5+ELEMENT_WIDTH,0.5,
                              ELEMENT_WIDTH)

# Directory structure and loading data
# defined from repository root
directory_base_name="wavetoy/wavetoy_so_{}{}_sinex_rho_{}"
subdirectories="output-0000/wavetoy_so_{}{}_sinex"
grid_file = "grid-coordinates.x.h5"
file_of_interest="ml_wavetoy_{}{}-wt_erru.x.h5"

# Keys
grid_key=u'GRID::x it=0 tl=0 rl=0'
erru_key=u'ML_WAVETOY_{}{}::erru it={} tl=0 rl=0'

def get_filepaths(t):
    """
    Given a stencil type
    gets the error and grid filenames
    for a simulation.
    """
    datafile= (directory_base_name.format(t,ORDER,RHO)
               +'/'+subdirectories.format(t,ORDER)
               +'/'+file_of_interest.format(t,ORDER))
    gridfile= (directory_base_name.format(t,ORDER,RHO)
               +'/'+subdirectories.format(t,ORDER)
               +'/'+grid_file)
    return datafile,gridfile

def get_data(t):
    """
    Given a stencil type
    gets x, erru data
    """
    datafilename,gridfilename=get_filepaths(t)
    datafile=h5py.File(datafilename,'r')
    gridfile=h5py.File(gridfilename,'r')
    x = gridfile[grid_key].value
    y = datafile[erru_key.format(t.upper(),ORDER,IT)].value
    datafile.close()
    gridfile.close()
    return x,y

grid,errus = get_data('dg')
elements = dge.get_elements_from_field(ORDER,
                                       grid,
                                       errus)


grids_interp = [None for i in range(NUM_SUBFIGS)]
errus_interp = [None for i in range(NUM_SUBFIGS)]
continuities = [False,True]
for i,cont in enumerate(continuities):
    grids_interp[i],errus_interp[i] =\
        dge.get_domain_interpolation(elements,
                                     cont)
    errus_interp[i] *= 10**SCALE_FACTOR

grid = np.hstack([e.get_x() for e in elements])
errus = [None for c in continuities]
errus[0] = np.hstack([e.get_y(dge.NARROW) for e in elements])
errus[1] = np.hstack([e.get_y(dge.AVERAGED) for e in elements])

f,axarr = plt.subplots(NUM_SUBFIGS,sharex=True)
ymin,ymax=-4E1,4E1
yspace = np.linspace(ymin,ymax,10)
for i in range(NUM_SUBFIGS):
    axarr[i].plot(grids_interp[i],errus_interp[i],'b-',lw=LINEWIDTH)
    axarr[i].plot(grid,
                  (10**SCALE_FACTOR)*errus[i],
                  'ro',lw=LINEWIDTH)
    axarr[i].set_ylabel(r'$E[\phi]\times 10^%s$' % SCALE_FACTOR,
                        fontsize=FONTSIZE)
    axarr[i].set_ylim(ymin,ymax)
    for pos in ELEMENT_POSITIONS:
        axarr[i].plot(pos*np.ones_like(yspace),yspace,'k:',lw=2)

axarr[0].set_title('without averaging')
axarr[1].set_title('with averaging')        

plt.xlim(XMIN,XMAX)
plt.xlabel('x',fontsize=FONTSIZE)

plt.savefig('wavetoy_average_no_average_comparison.pdf',
            bbox_inches='tight')

