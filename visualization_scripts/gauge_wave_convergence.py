#!/usr/bin/env python2

# gauge_wave_convergence.py
# Author: Jonah Miller (jonah.maxwell.miller@gmail.com)
# Time-stamp: <2016-03-30 00:14:26 (jmiller)>

# Add necessary libraries
import sys
sys.path.append('./visualization_scripts')

# imports
import numpy as np
import matplotlib as mpl
from matplotlib import pyplot as plt
from matplotlib import gridspec
import h5py
import dg_elements as dge
from scipy import optimize
from nostdout import *
import gc

# plotting settings
fontsize=16
linewidth=1
linewidth2=2
linewidth3=3
markersize=8
tlim=[0,4.0]
spacing=0.05
plot_pointwise_errors=True
plot_2norm_errors=True
demand_continuous=False
pointwise_fname='trunc_rho_bssn_dg{}_gauge_wave_pointwise_errors.pdf'
l2_fname = 'trunc_rho_bssn_dg{}_gauge_wave_l2_errors.pdf'
max_ticks=3

WIDTH = 1
A = .01
K = 2*np.pi/WIDTH
OMEGA = np.sqrt(K**2)
ORDER = 4
RHOS = [16,32,64]
FACTORS = RHOS
NUM_COMPONENTS = [4,8,16]
SCALE_FACTORS = map(lambda x: (2*x)**ORDER,RHOS)
T = 3.75
ITS = 750
XMIN=-0.5
XMAX=0.5
BREAKPOINT=0.46
TMIN=0
TMAX=4

par_name='gauge_wave'
test_type='gauge_wave'
directory_base_name=(test_type + '/'
                     +'trunc_rho_bssn_dg{}_'
                     +test_type
                     +'_rho_{}' + '/'
                     +'output-0000' + '/'
                     +par_name + '/')
grid_filename='grid-coordinates.x.h5'
data_filename="admbase-metric.x.h5"
grid_dsetname=u'GRID::x it=0 tl=0 rl=0 c={}'
data_dsetname=u'ADMBASE::gxx it={} tl=0 rl=0 c={}'

def gauge_wave_gxx(x,t):
    """
    The analytic form of the gxx component of a gauge_wave as a
    function of x and t. It is independent of y and z.
    """
    return 1 - A*np.sin(2*np.pi*(x-t)/WIDTH)

def get_data_of_type(t,order,resolutions):
    """
    Opens h5 files for resolutions 
    with filename t from directory
    structure
    """
    filenames=map(lambda x: directory_base_name.format(order,x)+t, 
              resolutions)
    files=map(lambda x: h5py.File(x,'r'),filenames)
    return files

def get_timesteps(datafile):
    relevant_keys = filter(lambda x: 'ADMBASE::gxx' in x, datafile.keys())
    timesteps = [None for key in relevant_keys]
    time_map = {}
    for i,key in enumerate(relevant_keys):
        timesteps[i] = datafile[key].attrs[u'timestep']
        time_map[timesteps[i]] = datafile[key].attrs[u'time']
    timesteps=list(set(timesteps))
    timesteps.sort()
    times = [time_map[t] for t in timesteps]
    times = map(lambda x: round(x,10),times)
    return timesteps,times

def close_fileset(fileset):
    for i in range(len(RHOS)):
        fileset[i].close()
    return

def get_data_at_timestep(it,
                         data_file,
                         num_components):
    "Gets data from file at iteration given num_components"
    f = data_file
    ddsets = [None for c in range(num_components)]
    ddsets[0] = f[data_dsetname.format(it,0)][:-1]
    ddsets[-1] = f[data_dsetname.format(it,num_components-1)][1:]
    if num_components > 2:
        for c in range(1,num_components-1):
            ddsets[c] = f[data_dsetname.format(it,c)][1:-1]
    data = np.hstack(ddsets)
    return data

def get_l2_convergence_at(it,time,
                          order, grid, data_file,
                          num_components,
                          demand_continuous = False):
    """
    Loads in the data for resolution rho and the appropriate order at
    iteration it and calculates the L2 error of each
    resolution. Returns three numbers: the L2-norm of the error for
    each resolution.
    """
    y = lambda x: gauge_wave_gxx(x,time)
    data = get_data_at_timestep(it,data_file,num_components)
    with nostdout():
        elements = dge.get_elements_from_field(order,grid,data)
    error = dge.get_L2_error(elements,y,demand_continuous)
    return error

def get_l2_convergence_of_time(order,grid,data_file,
                               num_components,
                               demand_continuous = False):
    timesteps,times = get_timesteps(data_file)
    errors = [None for time in times]
    for i,it in enumerate(timesteps):
        errors[i] = get_l2_convergence_at(it,times[i],
                                          order,grid,data_file,
                                          num_components,
                                          demand_continuous)
    return np.array(timesteps),np.array(times),np.array(errors)

# Load the grids
grid_files = get_data_of_type(grid_filename,ORDER,RHOS)
grids = [None for g in grid_files]
for i,f in enumerate(grid_files):
    dsets = [None for c in range(NUM_COMPONENTS[i])]
    dsets[0] = f[grid_dsetname.format(0)][:-1]
    dsets[-1] = f[grid_dsetname.format(NUM_COMPONENTS[i]-1)][1:]
    if NUM_COMPONENTS[i] > 2:
        for c in range(1,NUM_COMPONENTS[i]-1):
            dsets[c] = f[grid_dsetname.format(c)][1:-1]
    grids[i] = np.hstack(dsets)
close_fileset(grid_files)
coarsest_grid = grids[0]

# Open data files
data_files = get_data_of_type(data_filename,ORDER,RHOS)

# make fixed_time plot
if plot_pointwise_errors:
    its = map(lambda x: ITS*x,FACTORS)
    data = [None for rho in RHOS]
    elements = [None for rho in RHOS]
    xs = [None for rho in RHOS]
    ys = [None for rho in RHOS]
    errors = [None for rho in RHOS]
    for i,f in enumerate(data_files):
        data[i] = get_data_at_timestep(its[i],f,NUM_COMPONENTS[i])
        with nostdout():
            elements[i] = dge.get_elements_from_field(ORDER,grids[i],data[i])
        xs[i],ys[i] = dge.get_domain_interpolation(elements[i],demand_continuous)
        errors[i] = ys[i] - gauge_wave_gxx(xs[i],T)
        errors[i] *= SCALE_FACTORS[i]

    fig = plt.figure()
    gs = gridspec.GridSpec(1,2,width_ratios=[3,1])
    gs.update(hspace=spacing,wspace=spacing)
    ax0 = fig.add_subplot(gs[0])
    ax0.plot(xs[0],errors[0],'b-',linewidth=linewidth)
    ax0.plot(xs[1],errors[1],'g-',linewidth=linewidth)
    ax0.legend(['{} elements'.format(2*rho) for rho in RHOS])
    ax0.set_xlim(XMIN,BREAKPOINT)
    ax0.set_ylabel(r'$\Delta \gamma_{xx}/h^%s$' % ORDER,fontsize=fontsize)
    ax0.set_xlabel(r'$x$',fontsize=fontsize)
    ax0.text(-0.475,0.012,'t = {}'.format(T),fontsize=fontsize)
    ax1 = fig.add_subplot(gs[1],sharey=ax0)
    plt.setp(ax1.get_yticklabels(),visible=False)
    ax1.plot(xs[0],errors[0],'b-',linewidth=linewidth2)
    ax1.plot(xs[1],errors[1],'g-',linewidth=linewidth2)
    ax1.set_xlim(BREAKPOINT,XMAX)
    ax1.xaxis.set_major_locator(plt.MaxNLocator(max_ticks))
    plt.savefig(pointwise_fname.format(ORDER),
                bbox_inches='tight')
    plt.clf()

if plot_2norm_errors:
    timesteps = [None for rho in RHOS]
    times = [None for rho in RHOS]
    errors = [None for rho in RHOS]
    for i,f in enumerate(data_files):
        timesteps[i],times[i],errors[i] \
            = get_l2_convergence_of_time(ORDER,grids[i],f,
                                         NUM_COMPONENTS[i],
                                         demand_continuous)
        errors[i] *= SCALE_FACTORS[i]
#        errors[i] *= np.sqrt(2*RHOS[i])*SCALE_FACTORS[i]
#     lines = [plt.plot(times[i],errors[i],linewidth=linewidth3)\
#              for i in range(len(RHOS))]
    plt.plot(times[0],errors[0],'b-',linewidth=linewidth3)
    plt.plot(times[1],errors[1],'wo',markersize=markersize*2,alpha=0.5,
             mew=3)
    plt.plot(times[2],errors[2],'go',markersize=markersize)
    plt.legend(['{} elements'.format(2*rho) for rho in RHOS])
    plt.xlim(TMIN,TMAX)
    plt.xlabel(r'time ($1/c$)',fontsize=fontsize)
    plt.ylabel(r'$\|\Delta \gamma_{xx}\|_2/h^%s$' % ORDER,
               fontsize=fontsize)
    plt.ylim(0,0.01)
    plt.savefig(l2_fname.format(ORDER),
                bbox_inches='tight')

# close data files
close_fileset(data_files)
