#!/usr/bin/env python2

# Author: Jonah Miller (jonah.maxwell.miller@gmail.com)
# Time-stamp: <2016-03-28 15:16:52 (jmiller)>

# This script analyzes the wavetoy data and generates convergence
# plots for h- and p-refinment.

# imports
import numpy as np
import matplotlib as mpl
from matplotlib import pyplot as plt
import os
from scipy import optimize

# Global constants
FONTSIZE=16
FONT_WEIGHT='normal'
LINEWIDTH=3
MARKERSIZE=20
USETEX=True
DISPLAY_PLOTS=False
# output step that we read data from
# ranges from 0 to 21. Steps are the same accross h- and p-refinement
load_step = 15

# Directory structure and loading data
# defined from repository root
directory_base_name="wavetoy/wavetoy_so_dg{}_sinex_rho_{}"
subdirectories="output-0000/wavetoy_so_dg{}_sinex"
file_of_interest="ml_wavetoy_dg{}-wt_erru.norm2.asc"
def get_filepath(order,rho):
    """
    Given a stencil order and resolution rho, 
    gets the error filename
    for a simulation.
    """
    return (directory_base_name.format(order,rho)
            +'/'+subdirectories.format(order)
            +'/'+file_of_interest.format(order))
def get_err(order,rho):
    """Given a stencil order and resolution rho,
    returns the norm2 error in the energy as a function
    of time."""
    return np.loadtxt(get_filepath(order,rho))

def best_fit_decayh(rhos,errs,guess=(-4,2)):
    """
    Calculates the best fit to a power law for errs(rhos)
    Returns slope m, offset b

    Defined for h-refinement
    """
    print "Best fit for h-refinement."
    (m,b),pcov = optimize.curve_fit(lambda x,m,b: b*(x**m),
                                   rhos,errs,p0=guess)
    perr = np.sqrt(np.diag(pcov))
    print "[slope, offset] = [{}, {}]".format(m,b)
    print "1 sigma errors:\n{}".format(perr)
    return m,b

def best_fit_decayp(rhos,errs,guess=(-4,2)):
    """
    Calculates the best fit to a power law for errs(rhos)
    Returns slope m, offset b

    Defined for p-refinement
    """
    print "Best fit for p-refinement."
    line = lambda x,m,b: m*x + b

    lnerrs = np.log(errs)
    (m,b),pcov = optimize.curve_fit(line,rhos,lnerrs)
    
    perr = np.sqrt(np.diag(pcov))
    print "[slope, offset] = [{}, {}]".format(m,b)
    print "1 sigma errors:\n{}".format(perr)
    return m,b

def plot_h_refinement():
    """
    Generates plot for h-refinement
    """
    # load data
    order=4
    rhos=np.array([8,16,32,64])
    print "Time = {}".format(get_err(order,rhos[0])[load_step,1])
    errs=[get_err(order,rho)[load_step,2] for rho in rhos]

    # set fonts, etc
    mpl.rcParams['text.usetex'] = USETEX
    mpl.rcParams['font.size']=FONTSIZE
    mpl.rcParams['font.weight']=FONT_WEIGHT
    m,b = best_fit_decayh(rhos,errs)

    # generate data points and fit
    plt.loglog(rhos,(2.6*rhos**(-4.)),'b--',
               linewidth=LINEWIDTH)
    plt.loglog(rhos,errs,'ro',
               markersize=MARKERSIZE)

    # labels, ticks, etc
    plt.xlim(7,70)
    plt.xticks(rhos,rhos)
    plt.xlabel('number of elements',
               fontsize=FONTSIZE)
    plt.ylabel(r'$\|E[\phi]\|_2$')
    plt.legend([r'$\sim h^{-4}$','measured'],loc='upper right',
               fontsize=FONTSIZE)
    plt.savefig('trunc_rho_bssn_dg4_h_refinement.pdf',
                bbox_inches='tight')
    if DISPLAY_PLOTS:
        plt.show()
    else:
        plt.close()
    return
    
def plot_p_refinement():
    """
    Generates the plot for p-refinement
    """
    # load data
    rho=8
    orders=np.array([2,4,6,8])
    print "Time = {}".format(get_err(orders[0],rho)[load_step,1])
    errs=[get_err(order,rho)[load_step,2] for order in orders]

    # set fonts, etc.
    mpl.rcParams['text.usetex'] = USETEX
    mpl.rcParams['font.size']=FONTSIZE
    mpl.rcParams['font.weight']=FONT_WEIGHT

    m,b = best_fit_decayp(orders,errs)
    plt.semilogy(orders,np.exp(b)*np.exp(m*orders),'b--',
                 linewidth=LINEWIDTH)
    plt.semilogy(orders,errs,'ro',
                 markersize=MARKERSIZE)

    plt.xlim(1,9)
    ticknames=[2,4,6,8]
    plt.xticks(ticknames,ticknames)
    plt.xlabel('order')
    plt.ylabel(r'$\|E[\phi]\|_2$')
    plt.legend([r'$b e^{-a p}$','measured'],fontsize=FONTSIZE)
    plt.savefig('trunc_rho_bssn_dg_p_refinement.pdf',
                bbox_inches='tight')
    if DISPLAY_PLOTS:
        plt.show()
    else:
        plt.close()
    return

if __name__ == "__main__":
    plot_h_refinement()
    plot_p_refinement()
    
