#!/usr/bin/env python2

# gauge_wave_3D_convergence.pu
# Author: Jonah Miller (jonah.maxwell.miller@gmail.com)
# Time-stamp: <2016-03-28 15:18:22 (jmiller)>

# Imports
import numpy as np
import h5py
import matplotlib as mpl
import matplotlib.pyplot as plt

# plotting setup
FONTSIZE=16
MARKERSIZE=8
LINEWIDTH=3
plotname='trunc_rho_bssn_{}{}_gauge_wave_3d_norm2_error_of_t.pdf'

# simulation parameters
FD=False
order=8
typeflag = 'fd' if FD else 'dg'
typeflag_big = 'FD' if FD else 'DG'
base_factor=170

#rhos=np.array([2,3,4])
#num_components=np.array([8,40,128])
rhos=np.array([1,2,3])
num_components=np.array([4,8,40])

num_elements = 2*rhos
tfactors= base_factor*rhos
power=order
hs = 1./num_elements
factors = (1./hs)**power

# filesystem details
parameter_file_name='gauge_wave'
variable='metric'
varname='gxx'
display_name=r'$\gamma_{xx}$'
file_template='admbase-'
key_template='ADMBASE'
var_exact='metric_exact'
vname_exact='gExact11'
file_template_exact='gaugewave'
key_template_exact = 'GAUGEWAVE'
base_simname="gauge_wave_3D/trunc_rho_bssn_{}{}_gauge_wave_3D_rho_{}"
weights_key = u'CARPETREDUCE::weight it={} tl=0 rl=0 c={}'
metric_key = (u'{}::{}'.format(key_template,varname)
              +' it={} tl=0 rl=0 c={}')
exact_key = (u'{}::{}'.format(key_template_exact,
                             vname_exact)
             +' it={} tl=0 rl=0 c={}')
time_name = u'time'
timestep_name = u'timestep'
weights_prefix='CARPETREDUCE::weight'

# Convenient index names
METRIC=0
WEIGHTS=1
EXACT=2

# acess functions
def make_names(rho):
    "Given a resolution rho, generates filenames"
    simname = base_simname.format(typeflag,order,rho)
    directory = simname+"/output-0000/{}/".format(parameter_file_name)
    metric_filename = file_template+variable+'.xyz.file_{}.h5'
    weights_filename = 'carpetreduce-weight.xyz.file_{}.h5'
    exact_filename = file_template_exact+'-'+var_exact+'.xyz.file_{}.h5'
    return directory,metric_filename,weights_filename,exact_filename

def get_files(rho,num_components):
    """For a given resolution rho and 
    number of files num_components,
    opens the appropriate hdf5 files
    """
    directory,metric_filename,weights_filename,exact_filename = make_names(rho)

    tfile=h5py.File(directory+metric_filename.format(0))
    ftype=type(tfile)
    tfile.close()
    
    metric_files = np.empty(num_components,dtype=ftype)
    weights_files = np.empty_like(metric_files)
    exact_files = np.empty_like(metric_files)
    
    for i in range(num_components):
        metric_files[i] = h5py.File(directory+metric_filename.format(i))
        weights_files[i] = h5py.File(directory+weights_filename.format(i))
        exact_files[i] = h5py.File(directory+exact_filename.format(i))
        
    fileset = [metric_files,weights_files,exact_files]
    
    return fileset

def close_files_in_list(file_list):
    map(lambda x: x.close(),file_list)
    return

def close_files_in_set(fileset):
    map(close_files_in_list,fileset)
    return

def get_timesteps(fileset):
    datafile = fileset[1][0]
    relevant_keys = filter(lambda x: weights_prefix in x, datafile.keys())
    timesteps = [None for key in relevant_keys]
    time_map = {}
    for i,key in enumerate(relevant_keys):
        timesteps[i] = datafile[key].attrs[timestep_name]
        time_map[timesteps[i]] = datafile[key].attrs[time_name]
    timesteps=list(set(timesteps))
    timesteps.sort()
    times = [time_map[t] for t in timesteps]
    times = map(lambda x: round(x,10),times)
    return timesteps,times

def get_var_in_component(it,component,var_key,file_list):
    var = file_list[component][var_key.format(it,component)].value
    return var

def get_all_vars_in_component(it,component,fileset):
    metric = get_var_in_component(it,component,
                                  metric_key,fileset[METRIC])
    weights = get_var_in_component(it,component,
                                   weights_key,fileset[WEIGHTS])
    exact = get_var_in_component(it,component,
                                exact_key,fileset[EXACT])
    return metric,weights,exact
    

def get_norm2_error_in_component(it,component,fileset):
    metric,weights,exact = get_all_vars_in_component(it,component,
                                                    fileset)
    error = metric-exact
    norm2 = np.sqrt(np.sum(error*weights*error))
    norm2 /= np.sqrt(np.sum(weights))
    return norm2

def get_norm2_error_at_it(it,fileset,num_components):
    errors = [get_norm2_error_in_component(it,component,fileset)\
             for component in range(num_components)]
    out = map(lambda x: x**2,errors)
    out = reduce(lambda x,y: x+y,out)
    out = np.sqrt(out)
    out /= np.sqrt(num_components)
    return out

def get_norm2_error_of_time(rho,num_components):
    fileset = get_files(rho,num_components)
    timesteps,times = get_timesteps(fileset)
    errors = np.empty_like(times)
    for i,it in enumerate(timesteps):
        errors[i] = get_norm2_error_at_it(it,fileset,num_components)
    close_files_in_set(fileset)
    return times,errors,timesteps

# Extract the data
times_list = [None for rho in rhos]
errors_list = [None for rho in rhos]
timesteps_list = [None for rho in rhos]
for i,rho in enumerate(rhos):
    times_list[i],errors_list[i],timesteps_list[i]\
    = get_norm2_error_of_time(rho,num_components[i])

# Generate the plot
plt.plot(times_list[0],factors[0]*errors_list[0],'b-',lw=LINEWIDTH)
plt.plot(times_list[1],factors[1]*errors_list[1],'g--',lw=LINEWIDTH)
plt.plot(times_list[2],factors[2]*errors_list[2],'r:',lw=LINEWIDTH)

tmax = 4.#np.min([np.max(time) for time in times_list])
plt.xlim(0,tmax)
plt.xlabel(r'time ($1/c$)',fontsize=FONTSIZE)
plt.ylabel(r'$|\Delta$'+display_name+r'$|_2/h^{%s}$' % power,
           fontsize=FONTSIZE)
plt.ticklabel_format(style='sci', axis='y', scilimits=(0,0))
plt.legend([r'$%s^3$ elements' % ne for ne in num_elements],
          loc='lower center')
plt.savefig(plotname.format(typeflag,order),
            bbox_inches='tight')

