#!/usr/bin/env python2

# shifted_gauge_wave_1_plus_log_convergence.py
# Author: Jonah Miller (jonah.maxwell.miller@gmail.com)
# Time-stamp: <2016-03-22 12:16:23 (jmiller)>

# Add necessary libraries
import sys
sys.path.append('./visualization_scripts')

# imports
import numpy as np
import matplotlib as mpl
from matplotlib import pyplot as plt
import h5py
import dg_elements as dge
from scipy import optimize
from nostdout import *

# plotting settings
fontsize=16
linewidth=3
markersize=8
tlim=[0,4.0]
plot_waveform=True
plot_order4_errors=True
plot_convergence_orders=True
demand_continuous=False

# parameters
domain = 1. # width of domain
orders = [4,8] 
# row is order, column is (1/2) number of elements
resolutions = np.array([[32,64,96],
                        [3,6,9]],
                       dtype=int)
# number of memory contexts in simulation
num_components = np.array([[2,2,12],
                           [4,8,16]],
                          dtype=int)
# Timestep factors
factors = np.array([[1,2,3],
                    [1,2,3]],
                   dtype=int)

# number of elements
num_elements = 2*resolutions
# widths of elements
widths = domain/num_elements

test_type="shifted_gauge_wave_1plulog"
par_name="shifted_gauge_wave_1_plus_log"
directory_base_name=(par_name
                     +'/'
                     +"trunc_rho_bssn_dg{}_"
                     +test_type
                     +"_rho_{}/output-0000/"
                     +par_name
                     +"/")
grid_filename="grid-coordinates.x.h5"
data_filename="admbase-metric.x.h5"
grid_dsetname=u'GRID::x it=0 tl=0 rl=0 c={}'
data_dsetname=u'ADMBASE::gxx it={} tl=0 rl=0 c={}'

def scale_factor(h1,h2,h3,order):
    """
    Returns the scale factor for a self-convergence test.
    """
    return (h1**order - h3**order)/(h2**order - h3**order)

def individual_scale_factor(h1,h2,order):
    return h1**order - h2**order

def get_data_of_type(t,order,resolutions):
    """
    Opens h5 files for resolutions 
    with filename t from directory
    structure
    """
    filenames=map(lambda x: directory_base_name.format(order,x)+t, 
              resolutions)
    files=map(lambda x: h5py.File(x,'r'),filenames)
    return files

def get_timesteps(datafile):
    relevant_keys = filter(lambda x: 'ADMBASE::gxx' in x, datafile.keys())
    timesteps = [None for key in relevant_keys]
    time_map = {}
    for i,key in enumerate(relevant_keys):
        timesteps[i] = datafile[key].attrs[u'timestep']
        time_map[timesteps[i]] = datafile[key].attrs[u'time']
    timesteps=list(set(timesteps))
    timesteps.sort()
    times = [time_map[t] for t in timesteps]
    times = map(lambda x: round(x,10),times)
    return timesteps,times

def close_fileset(fileset):
    for i in range(resolutions.shape[0]):
        for j in range(resolutions.shape[1]):
            fileset[i][j].close()
    return

def get_all_timesteps(data_files):
    coarse_timesteps,coarse_times = get_timesteps(data_files[0])
    mid_timesteps,mid_times = get_timesteps(data_files[1])
    fine_timesteps,fine_times = get_timesteps(data_files[-1])
    itmax = np.min([len(coarse_timesteps),
                    len(mid_timesteps),
                    len(fine_timesteps)])
    steps = [coarse_timesteps,
             mid_timesteps,
             fine_timesteps]
    times = [coarse_times,
             mid_times,
             fine_times]
    return steps,times,itmax

def make_plot_at_it(order,it,grids,
                    data_files,
                    resolutions,
                    num_components,
                    factors,
                    coarse_timesteps,
                    coarse_times):
    print coarse_timesteps[it]
    data=get_data_at_timestep(data_files,
                              factors,
                              num_components,
                              coarse_timesteps[it])
    time_code=it#int(100*coarse_times[it])
    plt.plot(grids[-1],data[-1],'b-',lw=linewidth)
    #plt.plot(grids[1],data[1],'r--',lw=3)
    #plt.plot(grids[2],data[2],'go',lw=3)
    plt.xlabel('x',fontsize=fontsize)
    plt.ylabel(r'$\gamma_{xx}$',fontsize=fontsize)
    plt.text(-0.49,1.013,
             't = {}'.format(coarse_times[it]),
             fontsize=fontsize)
    plt.xlim(-.5,.5)
    #plt.ylim([0.9*ymin,1.1*ymax])
    # plt.legend(["{} elements".format(2*i) for i in resolutions],
    #            loc='lower right')
    plt.savefig('bssn_dg{}_gauge_wave_1_plus_log_f{:02d}_waveform.pdf'.format(order,
                                                                              time_code),
                bbox_inches='tight')
    plt.clf()
    return

def get_data_at_timestep(data_files,
                         factors,
                         num_components,
                         coarse_timestep):
    data = [None for f in data_files]
    for i,f in enumerate(data_files):
        step=coarse_timestep*factors[i]
        ddsets = [None for c in range(num_components[i])]
        ddsets[0] = f[data_dsetname.format(step,0)][:-1]
        ddsets[-1] = f[data_dsetname.format(step,num_components[i]-1)][1:]
        if num_components[i] > 2:
            for c in range(1,num_components[i]-1):
                ddsets[c] = f[data_dsetname.format(step,c)][1:-1]
        data[i] = np.hstack(ddsets)
    return data

def get_l2_self_convergence_at(order,it,
                               grids,
                               data_files,
                               factors,
                               num_components,
                               coarse_timesteps,
                               scale_factors=[1.0,1.0],
                               demand_continuous=False,
                               num_differences=2):
    """
    Loads in the data at iteration it and calculates
    the L2 difference between the coarse resolution simulations
    and the finest resolution. 
    
    Returns two numbers:
    L2(u_1 - u_3), and L2(u_2 - u_3)
    where u_1 is the coarsest resolution and u_3 is the finest
    """
    ts = coarse_timesteps[it]
    data=get_data_at_timestep(data_files,
                              factors,
                              num_components,
                              ts)
    elements = [None for d in data]
    for i,d in enumerate(data):
        elements[i] = dge.get_elements_from_field(order,grids[i],d)
    differences = [None for i in range(num_differences)]
    for i in range(num_differences):
        differences[i] = dge.get_lifted_differences(elements[i],
                                                    elements[2],
                                                    demand_continuous)
    l2s = [None for diff in differences]
    for i,diff in enumerate(differences):
        l2s[i] = dge.get_L2_norm(diff,scale_factors[i],demand_continuous)
    return l2s[0],l2s[1]

def calculate_convergence_order(coarse_differences,
                                fine_differences,
                                h1,h2,h3):
    """
    Takes l2_differences at one time for both
    a coarse resolution and a fine resolution and 
    calculates a convergence
    order using the secant method.
    
    The formula is
    coarse_differences/fine_differences = (h1^p - h3^p)/(h2^p-h3^p)
    where h1,h2,h3 are the widths of the elements 
    from coarsest to finest.
    
    code solves for p.
    
    returns p.
    """
    def discriminant(p):
        differences_ratio = coarse_differences/fine_differences
        h_ratio = (h1**p - h3**p)/(h2**p-h3**p)
        return differences_ratio - h_ratio
    root = optimize.newton(discriminant,order)
    return root

def plot_norm2_errors(order,itmax,coarse_times,
                      l2_differences,scale_factors,
                      num_elements):
    resmax = num_elements[-1]
    multiplier = np.sqrt(l2_differences.shape[0])
    f1,f2 = multiplier*scale_factors[0],multiplier*scale_factors[1]
    lines=[plt.plot(coarse_times[:itmax],l2_differences[...,0]/f1,'b-',lw=linewidth),
           plt.plot(coarse_times[:itmax],l2_differences[...,1]/f2,'r--',lw=linewidth)]
    plt.xlabel(r'Time ($1/c^3$)',fontsize=fontsize)
    plt.xlim(tlim[0],tlim[1])
    #string_completions = (resmax,order,resmax,order)
    string_completions = (order,order)
    plt.ylabel(r'$\frac{\|(\gamma_{xx})_i - (\gamma_{xx})_{3}\|_2}{\|h_{i}^%s - h_{3}^%s\|_2}$' % string_completions,
               fontsize=30)
    plt.legend(['{} elements'.format(num_elements[0]),
                '{} elements'.format(num_elements[1])],
               loc='lower center')
    plt.ticklabel_format(style='sci', axis='y', scilimits=(0,0))
    plt.savefig('trunc_rho_bssn_dg{}_shifted_gauge_wave_1plulog_self-convergence_over_time.pdf'.format(order),
                bbox_inches='tight')
    plt.clf()
    return

grid_files = [None for order in orders]
data_files = [None for order in orders]
for i,order in enumerate(orders):
    grid_files[i] =get_data_of_type(grid_filename,order,resolutions[i,...])
    data_files[i] = get_data_of_type(data_filename,order,resolutions[i,...])

grids = np.empty((len(orders),3),
                 dtype=np.ndarray)
for i,order in enumerate(orders):
    for j,f in enumerate(grid_files[i]):
        dsets = [None for c in range(num_components[i,j])]
        dsets[0] = f[grid_dsetname.format(0)][:-1]
        dsets[-1] = f[grid_dsetname.format(num_components[i,j]-1)][1:]
        if num_components[i,j] > 2:
            for c in range(1,num_components[i,j]-1):
                dsets[c] = f[grid_dsetname.format(c)][1:-1]
        grids[i,j] = np.hstack(dsets)

coarsest_grid = map(lambda g: g[0],grids)

scale_factors = np.empty((len(orders),2),dtype=float)
for i,order in enumerate(orders):
    scale_factors[i,0] = individual_scale_factor(widths[i,0],widths[i,2],order)
    scale_factors[i,1] = individual_scale_factor(widths[i,1],widths[i,2],order)

steps = [None for d in data_files]
times = [None for d in data_files]
itmaxs = [None for d in data_files]
for i,d in enumerate(data_files):
    steps[i],times[i],itmaxs[i] = get_all_timesteps(d)
coarse_timesteps = map(lambda x: x[0],steps)
coarse_times = map(lambda x: x[0],times)

for i,order in enumerate(orders):
    print "ORDER: {}".format(order)
    print "\tcoarse steps"
    for j in range(itmaxs[i]):
        print "\t\tt[{}] = t[{}] = t[{}] = {}".format(steps[i][0][j],
                                                      steps[i][1][j],
                                                      steps[i][2][j],
                                                      times[i][0][j])
        
data_to_plot = get_data_at_timestep(data_files[0],
                                    factors[0],
                                    num_components[0],
                                    steps[0][0][itmaxs[0]/2])
if plot_waveform:
    make_plot_at_it(orders[0],
                    itmaxs[0]/2,
                    grids[0],
                    data_files[0],
                    resolutions[0],
                    num_components[0],
                    factors[0],
                    coarse_timesteps[0],
                    coarse_times[0])

l2_differences = [np.empty((itmax,2),dtype=float) for itmax in itmaxs]
for i,order in enumerate(orders):
    for j in range(itmaxs[i]):
        with nostdout():
            l2_differences[i][j,0],l2_differences[i][j,1] \
                = get_l2_self_convergence_at(order,j,
                                             grids[i],data_files[i],
                                             factors[i],
                                             num_components[i],
                                             coarse_timesteps[i],
                                             demand_continuous=demand_continuous)

if plot_order4_errors:
    plot_norm2_errors(orders[0],itmaxs[0],
                      coarse_times[0],
                      l2_differences[0],
                      scale_factors[0],
                      num_elements[0])


if plot_convergence_orders:
    convergence_orders = [np.empty(l2_differences[i].shape[0],dtype=float)\
                          for i in range(len(orders))]
    for i in range(len(orders)):
        for j in range(l2_differences[i].shape[0]):
            convergence_orders[i][j] = calculate_convergence_order(l2_differences[i][j,0],
                                                                   l2_differences[i][j,1],
                                                                   widths[i,0],
                                                                   widths[i,1],
                                                                   widths[i,2])
    plt.plot(coarse_times[0][:itmaxs[0]],
             orders[0]*np.ones_like(convergence_orders[0]),'r--',
             lw=linewidth)
    plt.plot(coarse_times[0][:itmaxs[0]],
             convergence_orders[0],'ko',
             lw=linewidth)
    plt.plot(coarse_times[1][:itmaxs[1]],
             orders[1]*np.ones_like(convergence_orders[1]),'b:',
             lw=linewidth,markersize=markersize)
    plt.plot(coarse_times[1][:itmaxs[1]],
             convergence_orders[1],'g*',
             lw=linewidth,markersize=markersize)
    plt.xlim(tlim[0],tlim[1])
    plt.ylim([-1,orders[1]+3])
    plt.xlabel('Time ($1/c$)',fontsize=16)
    plt.ylabel('convergence order',fontsize=16)
    plt.legend(['{}'.format(orders[0])+r'$^{th}$'+'-order convergence',
                'measured convergence',
                '{}'.format(orders[1])+r'$^{th}$'+'-order convergence',
                'measured convergence'],
               loc='lower center')
    plt.savefig('trunc_rho_bssn_dgx_shifted_gauge_wave_1plulog_self-convergence_order.pdf',
                bbox_inches='tight')
    plt.clf()


close_fileset(grid_files)
close_fileset(data_files)
