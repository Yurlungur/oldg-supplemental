"""
extract_tensor_data_multipatch.py
Author: Jonah Miller (jonah.maxwell.miller@gmail.com)
Time-stamp: <2014-12-21 15:29:47 (jonah)>

This little library extends extract_tensor_data.py to be multipatch aware.

It uses both the grid::coordinates files and the data files
"""

# Imports
# ----------------------------------------------------------------------
import numpy as np
import extract_tensor_data as etd
import extract_coordinates_data as ecd
import simfactory_interface as interface
# ----------------------------------------------------------------------

# some simple wrappers
extract_data = etd.extract_data
make_coordinate_maps = ecd.make_coordinate_maps
WARNING_MESSAGE = etd.WARNING_MESSAGE

# Some important parameters
FILTER_DOMAIN_BOUNDARIES = False # For playing around. Don't use this.
DEBUGGING = False # For debugging
# Restart number for the data directory
RESTART_NUMBER =  interface.RESTART_NUMBER 

# For calculating the resolution
RESOLUTION_PARAMETER_STRING = "Coordinates::ncells_x"
DOMAIN_SIZE = 1

class Point:
    """
    A simple class for holding (x,y) data. Useful for sorting the data
    so that the x values are ordered.
    """
    def __init__(self,x,y):
        self.x = x
        self.y = y
        return
    def __cmp__(self,other):
        if self.x < other.x:
            return -1
        if self.x > other.x:
            return 1
        else:
            return 0

        
def filter_domain_boundaries(point_list):
    """
    The domain boundaries in DG methods are very error-prone and
    noisy. This filters them out to see how well the simulation is
    doing without the boundaries.

    The input should be a list of Point objects.
    """
    i = 0
    while i < len(point_list)-1:
        if point_list[i] == point_list[i+1]:
            point_list[i] = False
            point_list[i+1] = False
            i += 2
        else:
            i += 1
    point_list = filter(lambda x: bool(x),point_list)
    return point_list
    

def sort_list_pair(list1,list2):
    """
    Sometimes two lists are coupled: the ith element of list1
    corresponds to the ith element of list2. Thus, if we want to sort
    list1, we must rearrange list2 so that it still matches list1.
    """
    points = [Point(list1[i],list2[i]) for i in range(len(list1))]
    if FILTER_DOMAIN_BOUNDARIES:
        print "Filtering domain boundaries"
        points = filter_domain_boundaries(points)
    points.sort()
    list1 = [p.x for p in points]
    list2 = [p.y for p in points]
    return list1,list2
    
def element_of_position_at_snapshot(i,j,coord,snapshot,maps,time_index):
    """
    Returns two lists, position in the spacetime and the (i,j)th
    element of the tensor in the snapshot at that position.

    coord gives the coordinate to examine. 0=x, 1=y, 2=z

    Uses maps extracted from ecd.make_coordinate_maps

    time_index is the index of the snapshot.
    """
    positions = []
    elements = []
    for line in snapshot:
        if DEBUGGING:
            print "\t{}\t{}".format(maps[time_index][tuple(line[3])],
                                    etd.tensor_element(i,j,line[-1]))
        positions.append(maps[time_index][tuple(line[3])][coord])
        # positions.append(maps[time_index][coord][line[3][coord]]) # deprecated
        elements.append(etd.tensor_element(i,j,line[-1]))
    # positions and elements may not be sorted
    positions,elements = sort_list_pair(positions,elements)
    # Put them in numpy arrays for speed
    positions = np.array(positions)
    elements = np.array(elements)
    return positions,elements

def element_of_position_at_time(i,j,coord,time,data,maps):
    """
    Returns two lists, position in the spacetime, and the (i,j)th
    element of the tensor in dataset data at that position. Both are
    evaluated at the time given by time. This is number of computer
    iterations, not coordinate time.

    coord gives the coordinate to examine. 0=x,1=y=2=z

    Uses maps extracted from ecd.make_coordinate_maps
    """
    return element_of_position_at_snapshot(i,j,coord,data[time],maps,time)

def generate_map_resolution_and_tensor_data(directory_name,
                                            restart_number=RESTART_NUMBER,
                                            file_name=False):
    """
    Takes a directory name and returns map data, tensor data, and
    resolution data.

    File name is the name of the file containing the tensor data. By
    default it is the metric tensor.
    """
    # File path stuff
    paths = interface.get_file_paths(directory_name,restart_number,file_name)
    directory_path = paths[0]
    tensor_path = paths[1]
    coordinates_path = paths[2]
    parameter_path = paths[3]
    if DEBUGGING:
        print tensor_path
    # Extract the data
    tensor_data = extract_data(tensor_path)
    coordinate_maps = make_coordinate_maps(coordinates_path)
    number_of_cells = interface.extract_parameter_value(parameter_path,
                                                        RESOLUTION_PARAMETER_STRING,
                                                        True)
    # The resolution is the width of the coordinate space divided by
    # the number of cells
    resolution = float(DOMAIN_SIZE)/number_of_cells
    if DEBUGGING:
        print "num cells = {}".format(number_of_cells)
        print "h = {}".format(resolution)
    return tensor_data,coordinate_maps,resolution,number_of_cells


if __name__=="__main__":
    raise ImportWarning(WARNING_MESSAGE)
