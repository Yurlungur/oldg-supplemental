#!/usr/bin/env python2
# Author: Jonah Miller (jonah.maxwell.miller@gmail.com)

import numpy as np
import matplotlib as mpl
from matplotlib import pyplot as plt
import os

"""
Note that time units are:
(Solar Mass)*(Newton Constant)/(speed of light)^3

See:
https://einsteintoolkit.org/documentation/ThornDoc/EinsteinBase/HydroBase/

This gives a time of about 
4.93 microseconds. 

See:
http://www.wolframalpha.com/input/?i=%28Solar+Mass%29*%28Newton+Constant%29%2F%28speed+of+light%29^3
"""

FONTSIZE=16
LINEWIDTH=3
resolutions=[2,4,8]#[4,8,16]
order=4
directory_base_name="robust_stability/trunc_rho_bssn_dg{}_robust_stability_rho_{}/output-0000/robust_stability/"
file_of_interest="admbase-metric.norm2.asc"
files=map(lambda x: directory_base_name.format(order,x)+file_of_interest, resolutions)
data = map(lambda x: np.loadtxt(x),files)

times = [None for rho in resolutions]
gxys = [None for rho in resolutions]
#gxzs = [None for rho in resolutions]
#gyzs = [None for rho in resolutions]
for i,d in enumerate(data):
    times[i] = d[...,1]
    gxys[i] = d[...,3]*10**9
#    gxzs[i] = d[...,4]*10**9
#    gyzs[i] = d[...,6]*10**9

plt.ticklabel_format(style='sci', axis='y',useOffset=False)
lines = [plt.plot(time,gxy,linewidth=LINEWIDTH) for time,gxy in zip(times,gxys)]
lines[0][0].set_linestyle('-')
lines[1][0].set_linestyle('--')
lines[2][0].set_linestyle('-.')

plt.legend([r'$%s^3$ elements' % rho for rho in resolutions],loc='upper left')
plt.xlim(0,25)
plt.ylim(0,3)
#mpl.rcParams.update({'font.size': FONTSIZE})
plt.xlabel(r'time ($1/c$)',fontsize=FONTSIZE)
plt.ylabel(r'$\|\gamma_{xy}\|_2 \times 10^{-9}$',fontsize=FONTSIZE)
plt.savefig('robust_stability_dg{}_with_truncation_gxy_2norm_href.pdf'.format(order),
            bbox_inches="tight")


