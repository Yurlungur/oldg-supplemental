"""
generate_dg_interpolation.py
Author: Jonah Miller (jonah.maxwell.miller@gmail.com)
Time-stamp: <2016-02-25 22:08:35 (jmiller)>

This is a library that takes one-dimensional output from the einstein
toolkit (using discontinuous Galerkin methods) and generates an
interpolating polynomial to reconstruct the true solution.

There are two options. One can generate a piecewise smooth polynomial
OR a C1 polynomial whose DERIVATIVE is piecewise smooth (where the
discontinuities in the polynomial have been set to the average of the
left and right-hand limits.)

Also contains routines to plot data based on the interpolation.

UPDATE: Now adding capabilities to perform a fast Fourier transform
"""

# Imports
# ----------------------------------------------------------------------
import numpy as np # For array support
# For polynomial interpolation
import scipy.interpolate 
# Numerical integration
import scipy.integrate as integrate
# Orthogonal polynomials
import numpy.polynomial as polynomial
# linear algebra
from numpy import linalg
# For the operating system
import os
# Python tools
from copy import copy
# My toolbox
import extract_tensor_data_multipatch as multipatch
import simfactory_interface as interface
from dg_elements import *
# Plot tools
import matplotlib as mpl
import matplotlib.pyplot as plt
# ----------------------------------------------------------------------


# Global constants
# ----------------------------------------------------------------------
ORDER=4
COORD = 0 # we want to look at the x-coordinate
E_INDEX = (0,0) # We want to look at the xx coordinate of the metric
                # tensor. This variable doesn't do anything if we're
                # investigating a scalar wave.

# restart number for data directory
RESTART_NUMBER = interface.RESTART_NUMBER

# For calculating the resolution
RESOLUTION_PARAMETER_STRING = "Coordinates::ncells_x"
DOMAIN_SIZE = multipatch.DOMAIN_SIZE

# How many more points the interpolator should have compared to the
# original grid
INTERP_DENSITY=4

# some convenient names
maps_res_Txx = multipatch.generate_map_resolution_and_tensor_data
Txx_of_x_at_t = multipatch.element_of_position_at_time
# ----------------------------------------------------------------------


# Plotting Stuff
# ======================================================================
RESOLUTION = 200 # The number of data points we use for the analytic
                 # solution
LINEWIDTH = 3    # The line width in the plot
FONTSIZE = 22    # The fontsize in the plot
XLABEL = "Position"
TLABEL = "Time"
EXPONENT = -(ORDER) # Rescaling of the error by this exponent
RESCALE_ERRORS = True
ERR_LABEL_MODIFIER = " error" + r'$/h^4$'
TITLE_FORMAT="interpolated dg{} {}"
CONVERGENCE_TITLE = "Interpolated dg{} {}\nConvergence Order"
NORM_ERROR_TITLE="Normalized dg{} {} Error"
NORM_ERROR_YLABEL=r'$L_2$' + '({} error)'
SPECTRUM_TITLE = "Average {} Spectral Coefficient\nFor dg{} With {} Elements"
SPECTRUM_LABEL = "Spectral Coefficients"
XMIN=-0.5
XMAX=0.5
CONVERGENCE_ORDER_LABEL="log"+r'$_{(h2/h1)}(\|e_2\|_2/\|e_1\|_2)$'
NUM_TICKS=10
RESCALE_NORM2_ERRORS=True
# ======================================================================

    
# ======================================================================
# Data Analysis Methods
# ======================================================================
# ----------------------------------------------------------------------
# When filling elements with data, we need to remove the ghost cells
# that emerge from multi-processor calculations. This is a quick and
# dirty way to do it.
def find_ghost_cells(position):
    """
    Finds the indexes of cells in the position list which are ghost
    cells and contain redundant information. Returns a list of
    indices. 
    """
    ghost_indices = []
    for i in range(len(position)-1):
        if position[i-1] == position[i]\
                and position[i] == position[i+1]:
            ghost_indices.append(i)
    return ghost_indices


def delete_ghost_cells(position,Txx):
    """
    Given two arrays, position and Txx, returns two new arrays,
    position_new and Txx_new with the ghost cells deleted.
    """
    ghost_indices = find_ghost_cells(position)
    position_new = np.delete(position,ghost_indices)
    Txx_new = np.delete(Txx,ghost_indices)
    return position_new,Txx_new
# ----------------------------------------------------------------------


# ----------------------------------------------------------------------
def get_elements_from_Txx(order,position,Txx):
    """
    Takes a list of positions and tensor values and an element order
    and builds a list of elements containing the relevant
    information.

    Assumes that an element begins at the left boundary of the array
    and that the first point in the element is double valued.
    """
    # ensure element begins at the left boundary of the array
    assert position[0] == position[1]
    # delete any ghost cells
    position,Txx = delete_ghost_cells(position,Txx)
    # extract elements
    return get_elements_from_field(order,position,Txx)
# ----------------------------------------------------------------------


# ----------------------------------------------------------------------
def get_elements_at_time(time,order,directory_list,file_name=False):
    """
    Takes a time, an element order, a list of directories (strings)
    and generates several lists:

    ---- elements_matrix - A matrix of elements. Each row of the
                           matrix is a list of elements containing Txx
                           and position data for the data in a given
                           directory.

    -- time_index_list - Defines the time iteration at which the data
                         is extacted.

    -- h_list - Lists the average lattice spacing dx defined by the
                domain width and the number of elements.

    -- n_cells_list - Provides the number of cells.
    """
    time = float('%.10f' % time)
    elements_matrix = []
    time_index_list = []
    h_list = []
    num_cells_list = []
    for directory in directory_list:
        if multipatch.DEBUGGING:
            print directory
        tensor_data,coordinate_maps,h,num_cells =\
            maps_res_Txx(directory,RESTART_NUMBER,file_name)
        times = [float('%.10f' % snapshot[0][4]) for snapshot in tensor_data]
        # Raises an error if the appropriate time doesn't exist
        time_index = times.index(time)
        position,Txx=Txx_of_x_at_t(E_INDEX[0],
                                   E_INDEX[1],
                                   COORD,
                                   time_index,
                                   tensor_data,
                                   coordinate_maps)
        elements = get_elements_from_Txx(order,position,Txx)
        time_index_list.append(time_index)
        elements_matrix.append(elements)
        h_list.append(h)
        num_cells_list.append(num_cells/(order+1))
    return elements_matrix,time_index_list,h_list,num_cells_list
# ----------------------------------------------------------------------


# ----------------------------------------------------------------------
def get_L2_error_of_time(order,
                         analytic_function,
                         directory_list,
                         demand_continuous=False,
                         file_name=False):
    """
    Given an interpolation order, a list of directories (strings)
    containing solutions and an analytic function (vectorized a la
    numpy) describing what the solution should actually be, returns
    four lists:

    times_list --- contains arrays of times 

    L2_errors_list --- contains arrays of L2 error of the computed
                       solutions as a function of time.

    h_list --- contains the lattice spacing dx defined by the domain width
               and the number of elements

    n_cells_list --- provides the number of cells.

    The input analytic function is assumed to be of the form f(x,t).
    """
    # local variables
    times_list = []
    L2_errors_list = []
    h_list = []
    num_cells_list = []

    for directory in directory_list:
        if multipatch.DEBUGGING:
            print directory
        tensor_data,coordinate_maps,h,num_cells =\
            maps_res_Txx(directory,RESTART_NUMBER,file_name)
        num_cells /= (order+1)
        times = [float('%.10f' % snapshot[0][4]) for snapshot in tensor_data]

        L2_errors = [None for time in times]
        for time_index in range(len(times)):
            func = lambda x: analytic_function(x,times[time_index])
            position,Txx=Txx_of_x_at_t(E_INDEX[0],
                                       E_INDEX[1],
                                       COORD,
                                       time_index,
                                       tensor_data,
                                       coordinate_maps)
            elements = get_elements_from_Txx(order,position,Txx)
            L2_errors[time_index] = get_L2_error(elements,func,
                                                 demand_continuous)

        times_list.append(times)
        L2_errors_list.append(L2_errors)
        h_list.append(h)
        num_cells_list.append(num_cells)
    return times_list,L2_errors_list,h_list,num_cells_list
# ----------------------------------------------------------------------


# ----------------------------------------------------------------------
def get_modes_of_time(order,
                      directory,
                      normalized=True,
                      file_name=False):
    """
    Given an interpolation order, a directory (a string)
    containing numerical solutions, returns four objects:

    times --- An array of valud times

    modes_matrix --- Each row is the average FFT at a given time.

    h --- contains the lattice spacing dx defined by the domain width
          and the number of elements

    n_elements --- provides the number of elements

    The input analytic function is assumed to be of the form f(x,t).
    """
    tensor_data,coordinate_maps,h,num_cells =\
        maps_res_Txx(directory,RESTART_NUMBER,file_name)
    times = np.array([float('%.10f' % snapshot[0][4]) for snapshot in tensor_data])
    modes = np.empty((len(times),order+1))
    for time_index in range(len(times)):
        position,Txx=Txx_of_x_at_t(E_INDEX[0],
                                   E_INDEX[1],
                                   COORD,
                                   time_index,
                                   tensor_data,
                                   coordinate_maps)
        elements = get_elements_from_Txx(order,position,Txx)
        modes[time_index] = get_average_spectrum(elements,normalized)
    num_elements = num_cells / (order+1)

    return times,modes,h,num_elements
# ----------------------------------------------------------------------


# ----------------------------------------------------------------------
def get_mode_peaks(time,mode):
    """
    Given an array of times and the average mode per time for a single mode
    (each an array), find times and y-values of the peaks of the mode.
    """
    peak_time = []
    peak_mode = []
    # Extract all local maxima
    for i in range(1,len(time)-1):
        if mode[i] > mode[i+1] and mode[i] > mode[i-1] and mode[i] > 0:
            peak_time.append(time[i])
            peak_mode.append(mode[i])
    return peak_time,peak_mode
# ----------------------------------------------------------------------
# ======================================================================


# ======================================================================
# Plotting methods
# ======================================================================
# ----------------------------------------------------------------------
def plot_T_of_x(analytic_function,
                elements_matrix,
                num_cells_list,ylabel,
                time,
                outfile_name,
                demand_continuous=False):
    """
    Plots y(x) for each row of elements in element_matrix. Input variables:

    -- analytic_function, the true solution y(x,t). For comparison

    -- num_cells_list, the number of DG elements used in each simulation.
         Used for the legend.

    -- ylabel, the name we use for the y-axis

    -- time, the time slice we choose to plot.

    -- outfile_name, the name of the saved figure

    -- demand_continuous, whether or not the interpolation uses single
         or double-valued domain boundaries.
    """
    # theoretical data
    theoretical_positions = np.linspace(XMIN,XMAX,RESOLUTION)
    theoretical_Txx = analytic_function(theoretical_positions,time)

    # the domain interpolations
    x_list = [None for elements in elements_matrix]
    y_list = [None for elements in elements_matrix]
    for i in range(len(elements_matrix)):
        x_list[i],y_list[i] = get_domain_interpolation(elements_matrix[i],
                                                       demand_continuous)

    # Change the font size
    mpl.rcParams.update({'font.size': FONTSIZE})

    # Define the plots
    lines = [plt.plot(theoretical_positions,theoretical_Txx,linewidth=LINEWIDTH)]
    lines += [plt.plot(x_list[i],y_list[i],linewidth=LINEWIDTH)\
                  for i in range(len(x_list))]

    # Plot parameters
    num_elements_list = [len(elements) for elements in elements_matrix]
    plt.xlim([XMIN,XMAX])
    plt.xlabel(XLABEL)
    plt.ylabel(ylabel)
    plt.title(TITLE_FORMAT.format(ORDER,ylabel) + ", time = {}".format(time))
    plt.legend(["theory"] + ["{} Elements".format(i) for i in num_elements_list],
               fancybox=True,shadow=True,
               loc='center left',
               bbox_to_anchor=(1,0.5))
    plt.savefig(outfile_name,bbox_inches='tight')
    plt.show()
    return
# ----------------------------------------------------------------------


# ----------------------------------------------------------------------
def plot_errors(analytic_function,elements_matrix,
                num_cells_list,h_list,ylabel,time,
                outfile_name,
                demand_continuous=False):
    """
    Plots the error in computed y(x) for each row of elements in the
    element matrix. Input variables:

    -- analytic_function, the true solution y(x,t). For comparison

    -- num_cells_list, the number of DG elements used in each simulation.
         Used for the legend.

    -- h_list, approximate dx based on the width of an element

    -- ylabel, the name we use for the y-axis (without recaling)

    -- time, the time slice we choose to plot.

    -- outfile_name, the name of the saved figure

    -- demand_continuous, whether or not the interpolation uses single
         or double-valued domain boundaries.
    """
    # The domain interpolations
    # the domain interpolations
    x_list = [None for elements in elements_matrix]
    y_list = [None for elements in elements_matrix]
    for i in range(len(elements_matrix)):
        x_list[i],y_list[i] = get_domain_error(elements_matrix[i],
                                               lambda x: analytic_function(x,time),
                                               demand_continuous)

    if RESCALE_ERRORS:
        for i in range(len(elements_matrix)):
            y_list[i] *= (h_list[i]**EXPONENT)

    # Change font size
    mpl.rcParams.update({'font.size': FONTSIZE})

    # Define the plots
    lines = [plt.plot(x_list[i],y_list[i],linewidth=LINEWIDTH)\
                 for i in range(len(elements_matrix))]

    # Plot parameters
    num_elements_list = [len(elements) for elements in elements_matrix]
    plt.title(TITLE_FORMAT.format(ORDER,ylabel)+ERR_LABEL_MODIFIER +", time = {}".format(time))
    plt.xlabel(XLABEL)
    plt.ylabel(ylabel + ERR_LABEL_MODIFIER)
    plt.legend(["{} Elements".format(i) for i in num_elements_list],
               fancybox=True,shadow=True,
               loc='center left',
               bbox_to_anchor=(1,0.5))
    plt.savefig(outfile_name,bbox_inches='tight')
    plt.show()

    lines = [plt.plot(x_list[i],y_list[i],linewidth=LINEWIDTH)\
             for i in range(len(elements_matrix))]
    
    # Plot parameters
    #plt.title(TITLE_FORMAT.format(ORDER,ylabel)+ERR_LABEL_MODIFIER +", time = {}".format(time))
    plt.xlabel(XLABEL)
    plt.xlim([0.1,0.5])
    plt.ylabel(ylabel + ERR_LABEL_MODIFIER)
    plt.legend(["{} Elements".format(i) for i in num_cells_list],
               fancybox=True,shadow=True,
               loc='center left',
               bbox_to_anchor=(1,0.5))
    plt.savefig('cropped_' + outfile_name,bbox_inches='tight')
    plt.show()
    return
# ----------------------------------------------------------------------


# ----------------------------------------------------------------------
def convergence_order(errors1,errors2,times1,times2,h1,h2):
    """
    takes l2 errors of time for times1 and times2 and dx values h1 and
    h2 and computes
    n = log_(h2/h1)(|e_2|_2/|e_1|_2)
    returns two arrays, t,n(t)
    """
    shared_times = sorted(list(set.intersection(set(times1),set(times2))))
    restricted_errors1 = np.array([errors1[i] \
                                       for i in range(len(times1))\
                                       if times1[i] in shared_times])
    restricted_errors2 = np.array([errors2[i] \
                                       for i in range(len(times2))\
                                       if times2[i] in shared_times])
    ratio = restricted_errors2/restricted_errors1
    base = h2/h1
    estimated_order = np.log(ratio)/np.log(base)
    return shared_times, estimated_order
# ----------------------------------------------------------------------


# ----------------------------------------------------------------------
def plot_convergence_order(directory_list,analytic_function,
                           variable_name,outfile_name,
                           demand_continuous=False,
                           infile_name=False):
    """
    Given a list of directories and an analytic solution, plots the
    order of convergence of the calculated solutions in the
    directories as a function of time.

    infile_name determines the name of the file within a directory to be used.
    variable name determines the title

    The convergence order is approximately

    n = log_(h2/h1)(|e_2|_2/|e_1|_2)
    """
    times_list,L2_errors_list,h_list,num_cells_list = \
        get_L2_error_of_time(ORDER,analytic_function,
                             directory_list,demand_continuous,infile_name)
    title = CONVERGENCE_TITLE.format(ORDER,variable_name)
    ylabel = CONVERGENCE_ORDER_LABEL
    shared_times_list = [None for i in range(len(h_list))]
    n_list = [None for i in range(len(h_list))]
    for i in range(len(h_list)-1):
        shared_times_list[i],n_list[i]=convergence_order(L2_errors_list[i],
                                                         L2_errors_list[i+1],
                                                         times_list[i],
                                                         times_list[i+1],
                                                         h_list[i],
                                                         h_list[i+1])
    labels_list = [(r'$n_1$'+" = {} elements, "+r'$n_2$'+" = {} elements").format(num_cells_list[i], num_cells_list[i+1]) for i in range(len(h_list)-1)]
    # plot
    mpl.rcParams.update({'font.size':FONTSIZE})
    lines = [plt.plot(shared_times_list[i],n_list[i],linewidth=LINEWIDTH)\
                 for i in range(len(h_list))]
    plt.title(title)
    plt.ylabel(ylabel)
    plt.xlabel(TLABEL)
    plt.legend(labels_list,
               loc='upper center',bbox_to_anchor=(0.5,-0.1),
               fancybox=True,shadow=True,ncol=1)
    plt.savefig(outfile_name,bbox_inches='tight')
    plt.show()
    return
# ----------------------------------------------------------------------


# ----------------------------------------------------------------------
def plot_norm2_error(directory_list,analytic_function,
                     variable_name,outfile_name,
                     demand_continuous=False,
                     infile_name=False):
    """Given a list of directories and an analytic solution, plots the
    L^2 error of the calculated solutions in the directories as a
    function of time.

    infile_name determines the name of the file within a directory to be used.
    variable name determines the title
    """
    times_list,L2_errors_list,h_list,num_cells_list = \
        get_L2_error_of_time(ORDER,analytic_function,
                             directory_list,demand_continuous,infile_name)
    title = NORM_ERROR_TITLE.format(ORDER,variable_name)
    ylabel = NORM_ERROR_YLABEL.format(variable_name)
    if RESCALE_NORM2_ERRORS:
        for i,errors in enumerate(L2_errors_list):
            L2_errors_list[i] = map(lambda x: x * h_list[i]**EXPONENT, errors)
    labels_list = ["elements = {}".format(num_cells_list[i]) for i in range(len(num_cells_list))]
    # plot
    mpl.rcParams.update({'font.size':FONTSIZE})
    lines = [plt.plot(times_list[i],L2_errors_list[i],linewidth=LINEWIDTH)\
                 for i in range(len(h_list))]
    plt.title(title)
    plt.ylabel(ylabel)
    plt.xlabel(TLABEL)
    plt.legend(labels_list,
               fancybox=True,shadow=True,
               loc='center left',
               bbox_to_anchor=(1,0.5))
    plt.savefig(outfile_name,bbox_inches='tight')
    plt.show()
    return
# ----------------------------------------------------------------------


# ----------------------------------------------------------------------
def plot_peak_modes(time,mode,order,mode_number):
    """
    Given the output of peak modes, plot the peaks on a semilogy plot.
    Order and mode number given for title and saving purposes.
    """
    mpl.rcParams.update({'font.size':FONTSIZE})
    plt.semilogy(time,mode,'ro')
    plt.xlabel(TLABEL)
    plt.ylabel("Peak amplitudes of the spectrum.")
    plt.title("Peak Amplitudes of the Spectrum\nMode Number = {}\nOrder = {}".format(mode_number,order))
    plt.savefig("spectrum_peaks_dg{}_mode_number_{}.pdf".format(order,mode_number),
                bbox_inches='tight')
    plt.show()
    return
# ----------------------------------------------------------------------


# ----------------------------------------------------------------------
def plot_average_spectrum(directory,
                          variable_name,
                          outfile_name,
                          normalized=True,
                          infile_name=False,
                          hide_modes=False):
    """
    Given a directory, a variable name (for pretty plotting),
    and a filename to save the plot to, plots each of the normalized modes
    as a function of time.

    infile_name determines the name of the file within the directory to be used.

    if normalize=True, then the coefficients in the FFT are
    normalized. Otherwise the scale is absolute. Since
    I have not figured out the appropriate scaling relation for these
    modes, this behavior is unpdredictable.

    If hide_modes is set to a nonzero number, modes less than
    that number will be absent on the plot.
    """
    # Extract data
    times,modes,h,num_elements = get_modes_of_time(ORDER,
                                                   directory,
                                                   normalized,
                                                   infile_name)
    highest_order_mode = modes.shape[-1]-1
    if hide_modes:
        assert hide_modes >= 0
        modes = modes[...,hide_modes:]
    # Print some data analysis
    modemeans = np.mean(modes,axis=0)
    modemax = np.max(modes,axis=0)
    modemin = np.min(modes,axis=0)
    print "Max values: {}".format(modemax)
    print "Min values: {}".format(modemin)
    print "Mean values: {}".format(modemeans)
    # Set plot parameters
    title = SPECTRUM_TITLE.format("Normalized" if normalized else "Unnormalized",
                                  ORDER,
                                  num_elements)
    ylabel = SPECTRUM_LABEL
    xlabel = TLABEL
    labels_list = ["Mode Number: {}".format(i) for i in range(ORDER+1)]
    if hide_modes:
        labels_list = labels_list[hide_modes:]
    mpl.rcParams.update({'font.size':FONTSIZE})
    # plot
    lines = [plt.plot(times,modes[...,i],linewidth=LINEWIDTH)\
             for i in range(len(modes[0]))]
    ymax = max(modemax)
    ymin = min(modemin)
    plt.grid(b=True, which='major', color='b', linestyle='-')
    plt.yticks(np.linspace(ymin,ymax,NUM_TICKS))
    plt.title(title)
    plt.ylabel(ylabel)
    plt.xlabel(xlabel)
    plt.legend(labels_list,
               loc='center left',
               bbox_to_anchor=(1.0,0.5),
               fancybox=True,
               shadow=True,
               ncol=1)
    plt.savefig(outfile_name,bbox_inches='tight')
    plt.show()
    lines = [plt.semilogy(times,modes[...,i],linewidth=LINEWIDTH)\
             for i in range(len(modes[0]))]
    plt.title(title)
    plt.ylabel(ylabel)
    plt.xlabel(xlabel)
    plt.legend(labels_list,
               loc='center left',
               bbox_to_anchor=(1.0,0.5),
               fancybox=True,
               shadow=True,
               ncol=1)
    plt.savefig('semilogy_'+outfile_name,bbox_inches='tight')
    plt.show()
    # Plot the highest-order mode
    #peak_times,peak_modes = get_mode_peaks(times,(modes-modemeans)[...,-1])
    #plot_peak_modes(peak_times,peak_modes,ORDER,highest_order_mode)
    return
# ----------------------------------------------------------------------

    
# ----------------------------------------------------------------------
def make_pointwise_plots(time,directory_list,
                         analytic_function,
                         ylabel,plot_name,
                         error_name,
                         demand_continuous=False,
                         infile_name=False):
    """
    Given a list of directories and an anlaytic solution, plots the
    solution in the directories and its error compared to the analytic
    function.
    ylabel is the name of the variable that goes on the y axis.
    plot_name is the name you save the solution plot to.
    error_name is the name you save the error plots to.
    infile_name is the name of the file data is extracted from.
    """
    elements_matrix,time_index_list,h_list,num_cells_list=\
        get_elements_at_time(time,ORDER,directory_list,infile_name)
    plot_T_of_x(analytic_function,elements_matrix,num_cells_list,
                ylabel,time,plot_name)
    plot_errors(analytic_function,elements_matrix,num_cells_list,
                h_list,ylabel,time,error_name,demand_continuous)
# ----------------------------------------------------------------------

# ======================================================================


if __name__=="__main__":
    raise ImportWarning(multipatch.WARNING_MESSAGE)
