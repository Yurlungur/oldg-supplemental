#!/usr/bin/env python2

# gauge_wave_3D_sliceplot.py
# Author: Jonah Miller (jonah.maxwell.miller@gmail.com)
# Time-stamp: <2016-03-21 22:27:18 (jmiller)>

# Imports
import yt
import numpy as np
import os
import h5py
import matplotlib as mpl
import matplotlib.pyplot as plt

# plotting setup
FONTSIZE=26
plotname='gauge_wave_3D_slice_lowres.pdf'
CMAP='CMRmap_r'
bound = 0.5

# simulation parameters
ijk2c = np.empty((2,2,2),
                 dtype=int)
ijk2c[0,0,0]=0
ijk2c[1,0,0]=1
ijk2c[0,1,0]=2
ijk2c[1,1,0]=3
ijk2c[0,0,1]=4
ijk2c[1,0,1]=5
ijk2c[0,1,1]=6
ijk2c[1,1,1]=7

FD=False
order=8

typeflag = 'fd' if FD else 'dg'
typeflag_big = 'FD' if FD else 'DG'
base_factor=170
it = 0
rho = 2
num_components=8

tfactor = base_factor*rho

parameter_file_name='gauge_wave'

variable='metric'
varname='gxx'
display_name=r'$\gamma_{xx}$'
file_template='admbase-'
key_template='ADMBASE'

var_exact='metric_exact'
vname_exact='gExact11'
file_template_exact='gaugewave'
key_template_exact = 'GAUGEWAVE'


metric_key = (u'{}::{}'.format(key_template,varname)
              +' it={}'.format(it)+
              ' tl=0 rl=0 c={}')
exact_key = (u'{}::{}'.format(key_template_exact,
                             vname_exact)
             +' it={}'.format(it)
             +' tl=0 rl=0 c={}')
grid_key = (u'GRID::{} '
            +'it={}'.format(it)+
            ' tl=0 rl=0 c={}')

time_name = u'time'
timestep_name = u'timestep'
weights_prefix='CARPETREDUCE::weight'

base_simname="gauge_wave_3D/trunc_rho_bssn_{}{}_gauge_wave_3D_rho_{}"
simname = base_simname.format(typeflag,order,rho)
directory = simname+"/output-0000/{}/".format(parameter_file_name)
metric_filename = directory+file_template+variable+'.xyz.file_{}.h5'
grid_filename = directory+'grid-coordinates.xyz.file_{}.h5'

# Acess files
tfile=h5py.File(metric_filename.format(0))
ftype=type(tfile)
tfile.close()
    
metric_files = np.empty(num_components,dtype=ftype)
grid_files = np.empty_like(metric_files)

for i in range(num_components):
    metric_files[i] = h5py.File(metric_filename.format(i))
    grid_files[i] = h5py.File(grid_filename.format(i))

def get_coord_in_component(component,coord):
    coord = grid_files[component][grid_key.format(coord,component)].value
    return coord

def get_metric_in_component(component):
    metric = metric_files[component][metric_key.format(component)].value
    return metric

def get_boundaries_from_centers(quad):
    """
    Given a list of cell centers for a hexahedral mesh in 1D, 
    generates cell boundaries
    """
    out = np.empty((len(quad)-1))
    for i in range(len(quad)-2):
        out[i+1] = 0.5*(quad[i+1]+quad[i+2])
    out[0] = quad[0] - 0.5*(quad[2]-quad[1])
    out[-1] = quad[-1] + 0.5*(quad[-2]-quad[-3])
    return out

xquad = np.hstack([get_coord_in_component(0,'x')[0,0,...][:-1],
                   get_coord_in_component(1,'x')[0,0,...][1:]])
xgrid = get_boundaries_from_centers(xquad)

# some debugging output
for c in range(num_components):
    print "{}: ({},{},{})".format(c,
                                  get_coord_in_component(c,'x')[0,0,0],
                                  get_coord_in_component(c,'y')[0,0,0],
                                  get_coord_in_component(c,'z')[0,0,0])


# combine the metrics
metrics = np.empty((2,2,2),
                   dtype=np.ndarray)
for i in range(2):
    for j in range(2):
        for k in range(2):
            metrics[i,j,k]=get_metric_in_component(ijk2c[i,j,k])
metric_ribbons=np.empty((2,2),
                        dtype=np.ndarray)
for i in range(2):
    for j in range(2):
        metric_ribbons[i,j]=np.concatenate([metrics[i,j,0][...,...,:-1],
                                            metrics[i,j,1][...,...,1:]],axis=2)
metric_slices = np.empty(2,dtype=np.ndarray)
for i in range(2):
    metric_slices[i] = np.concatenate([metric_ribbons[i,0][...,:-1,...],
                                       metric_ribbons[i,1][...,1:,...]],axis=1)
metric = np.concatenate([metric_slices[0][:-1,...,...],
                         metric_slices[1][1:,...,...]],axis=0)

# Use low-order interpolation to convert from
# a vertex-centered grid to a cell-centered grid
for i in range(2):
    metric = 0.5 * (metric[:-1,:,:] + metric[1:,:,:])
    metric = 0.5 * (metric[:,:-1,:] + metric[:,1:,:])
    metric = 0.5 * (metric[:,:,:-1] + metric[:,:,1:])

metric = 1000*(metric - 1)

zmax = np.max(metric)
zmin = np.min(metric)
print zmin,zmax
zavg = (zmax+zmin)/2.
dz = (zmax - zmin)/2.

data = {variable:metric}

# Close the HDF5 Files
for i in range(num_components):
    metric_files[i].close()
    grid_files[i].close()

# load into yt
limits = [-bound,bound]
bbox = np.array([limits,limits,limits])
coords,conn = yt.hexahedral_connectivity(xgrid,xgrid,xgrid)
ds = yt.load_hexahedral_mesh(data,conn,coords,bbox=bbox,sim_time = 0)

# Tell yt about our metric field
def _gxx(field,data):
    return data[variable]
yt.add_field(varname,function=_gxx,display_name=r'$\gamma_{xx}$',
             force_override=True,units="")

# Generate the plot
p = yt.SlicePlot(ds,'z',varname,fontsize=FONTSIZE)
p.set_xlabel(r'$x$')
p.set_ylabel(r'$y$')
p.set_colorbar_label(varname,r'$(\gamma_{xx}-1)\times 10^4$')
p.set_cmap(field=varname,cmap=CMAP)
p.set_log(varname,False)
p.set_zlim(varname,zavg-1.2*dz,zavg+1.2*dz)

p.save(plotname,
      mpl_kwargs={'bbox_inches':'tight'})

