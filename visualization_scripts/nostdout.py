"""
nostdout() is a context function that surpresses function
output. Useful for hiding debugging output if you're too lazy to
modify the source code. See this stackoverflow comment:
http://stackoverflow.com/a/2829036
"""

import contextlib
import sys

class DummyFile(object):
    def write(self, x): pass

    def flush(self): pass

@contextlib.contextmanager
def nostdout():
    save_stdout = sys.stdout
    sys.stdout = DummyFile()
    yield
    sys.stdout = save_stdout
