#!/usr/bin/env python2

# Author: Jonah Miller (jonah.maxwell.miller@gmail.com)
# Time-stamp: <2016-03-21 19:27:11 (jmiller)>

# This compares pointwise error for FD and DG

# imports
import numpy as np
import matplotlib as mpl
from matplotlib import pyplot as plt
from scipy import interpolate
import h5py
import os
import dg_elements as dge

# Global constants
FONTSIZE=16
LINEWIDTH=3
ORDER=8
RHO=4
TYPES = ['fd','dg']
IT=60
TIME=0.75
demand_continuous=False
WIDTH = 1.#2*(1./8)
XMAX = 0.5*WIDTH
XMIN = -XMAX
INTERP_DENSITY=10
SCALE_FACTOR=8 # multiply everything by 10^SCALE_FACTOR

ELEMENT_WIDTH=1.0/RHO
ELEMENT_POSITIONS = np.arange(-0.5+ELEMENT_WIDTH,0.5,
                              ELEMENT_WIDTH)

interp = interpolate.BarycentricInterpolator

# Directory structure and loading data
# defined from repository root
directory_base_name="wavetoy/wavetoy_so_{}{}_sinex_rho_{}"
subdirectories="output-0000/wavetoy_so_{}{}_sinex"
grid_file = "grid-coordinates.x.h5"
file_of_interest="ml_wavetoy_{}{}-wt_erru.x.h5"

# Keys
grid_key=u'GRID::x it=0 tl=0 rl=0'
erru_key=u'ML_WAVETOY_{}{}::erru it={} tl=0 rl=0'

def get_filepaths(t):
    """
    Given a stencil type
    gets the error and grid filenames
    for a simulation.
    """
    datafile= (directory_base_name.format(t,ORDER,RHO)
               +'/'+subdirectories.format(t,ORDER)
               +'/'+file_of_interest.format(t,ORDER))
    gridfile= (directory_base_name.format(t,ORDER,RHO)
               +'/'+subdirectories.format(t,ORDER)
               +'/'+grid_file)
    return datafile,gridfile

def get_data(t):
    """
    Given a stencil type
    gets x, erru data
    """
    datafilename,gridfilename=get_filepaths(t)
    datafile=h5py.File(datafilename,'r')
    gridfile=h5py.File(gridfilename,'r')
    x = gridfile[grid_key].value
    y = datafile[erru_key.format(t.upper(),ORDER,IT)].value
    datafile.close()
    gridfile.close()
    return x,y

def get_interpolation_fd(x,y):
    """
    Given x and y values, uses RHO-order interpolation
    on a finer uniform grid
    """
    mx = x
    my = y
    interpolators = []
    grids = []
    data = []
    for i in range(0,len(mx),ORDER):
        # Use lower-order interpolation at
        # right boundary if need be.
        if len(mx[i:i+ORDER+1]) < ORDER+1:
            interpolators.append(interp(mx[i:],
                                        my[i:]))
            grids.append(np.linspace(mx[i],
                                     mx[-1],
                                     INTERP_DENSITY*ORDER+1))
        else:
            interpolators.append(interp(mx[i:i+ORDER+1],
                                        my[i:i+ORDER+1]))
            grids.append(np.linspace(mx[i],
                                     mx[i+ORDER],
                                     INTERP_DENSITY*ORDER+1))
        data.append(interpolators[-1](grids[-1]))
    data = np.hstack(data)
    grid = np.hstack(grids)
    return grid,data

grids = {t : None for t in TYPES}
errus = {t : None for t in TYPES}
grids_interp = {t : None for t in TYPES}
errus_interp = {t : None for t in TYPES}
interpolators = [None for i in range(RHO)]
for t in TYPES:
    grids[t],errus[t] = get_data(t)

elements = dge.get_elements_from_field(ORDER,
                                       grids['dg'],
                                       errus['dg'])

grids_interp['dg'],errus_interp['dg'] = \
        dge.get_domain_interpolation(elements,
                                     demand_continuous)


grids_interp['fd'],errus_interp['fd'] = \
        get_interpolation_fd(grids['fd'],errus['fd'])

for t in TYPES:
    errus[t] *= 10**SCALE_FACTOR
    errus_interp[t] *= 10**SCALE_FACTOR

f,axarr = plt.subplots(2,sharex=True)
ymin,ymax=-40,40
axarr[0].plot(grids_interp['dg'],errus_interp['dg'],'b-',lw=LINEWIDTH)
axarr[0].plot(grids['dg'],errus['dg'],'ro',lw=LINEWIDTH)
axarr[0].set_title('discontinuous Galerkin')
axarr[0].set_ylabel(r'$E[\phi]\times 10^%s$' % SCALE_FACTOR,
                    fontsize=FONTSIZE)
axarr[0].set_ylim(ymin,ymax)
yspace = np.linspace(ymin,ymax,10)
for pos in ELEMENT_POSITIONS:
    axarr[0].plot(pos*np.ones_like(yspace),yspace,'k:',lw=2)

axarr[1].plot(grids_interp['fd'],errus_interp['fd'],'b-',lw=LINEWIDTH)
axarr[1].plot(grids['fd'],errus['fd'],'ro',lw=LINEWIDTH)
axarr[1].set_title('finite differences')
axarr[1].text(0.65*XMAX,0.6,'t = {}'.format(TIME),
              fontsize=FONTSIZE)
axarr[1].set_ylabel(r'$E[\phi]\times 10^%s$' % SCALE_FACTOR,
                    fontsize=FONTSIZE)
axarr[1].set_ylim(-0.8,0.8)

plt.xlim(XMIN,XMAX)
plt.xlabel('x',fontsize=FONTSIZE)

plt.savefig('wavetoy_erru_dg_fd_comparison_8th_order.pdf',
            bbox_inches='tight')

