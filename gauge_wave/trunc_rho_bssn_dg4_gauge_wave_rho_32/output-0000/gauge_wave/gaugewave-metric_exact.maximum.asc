# Scalar ASCII output created by CarpetIOScalar
# created on nid25572 by miller1 on Mar 13 2016 at 10:07:48-0500
# parameter filename: "/scratch/sciteam/miller1/simulations/trunc_rho_bssn_dg4_gauge_wave_rho_32/output-0000/gauge_wave.par"
# Build ID: build-dgfe2-h2ologin1.ncsa.illinois.edu-miller1-2016.02.08-01.00.23-31758
# Simulation ID: run-gauge_wave-nid25572-miller1-2016.03.13-15.07.46-22102
# Run ID: run-gauge_wave-nid25572-miller1-2016.03.13-15.07.46-22102
#
# GAUGEWAVE::gExact11 (gaugewave-metric_exact)
# 1:iteration 2:time 3:data
# data columns: 3:gExact11 4:gExact12 5:gExact13 6:gExact22 7:gExact23 8:gExact33
0 0 1.01 0 0 1 0 1
1600 0.25 1.01 0 0 1 0 1
3200 0.5 1.01 0 0 1 0 1
4800 0.75 1.01 0 0 1 0 1
6400 1 1.01 0 0 1 0 1
8000 1.25 1.01 0 0 1 0 1
9600 1.5 1.01 0 0 1 0 1
11200 1.75 1.01 0 0 1 0 1
12800 2 1.01 0 0 1 0 1
14400 2.25 1.01 0 0 1 0 1
16000 2.5 1.01 0 0 1 0 1
17600 2.75 1.01 0 0 1 0 1
19200 3 1.01 0 0 1 0 1
20800 3.25 1.01 0 0 1 0 1
22400 3.5 1.01 0 0 1 0 1
24000 3.75 1.01 0 0 1 0 1
25600 4 1.01 0 0 1 0 1
27200 4.25 1.01 0 0 1 0 1
28800 4.5 1.01 0 0 1 0 1
30400 4.75 1.01 0 0 1 0 1
32000 5 1.01 0 0 1 0 1
