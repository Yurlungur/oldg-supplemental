# Scalar ASCII output created by CarpetIOScalar
# created on nid25572 by miller1 on Mar 13 2016 at 10:07:49-0500
# parameter filename: "/scratch/sciteam/miller1/simulations/trunc_rho_bssn_dg4_gauge_wave_rho_32/output-0000/gauge_wave.par"
# Build ID: build-dgfe2-h2ologin1.ncsa.illinois.edu-miller1-2016.02.08-01.00.23-31758
# Simulation ID: run-gauge_wave-nid25572-miller1-2016.03.13-15.07.46-22102
# Run ID: run-gauge_wave-nid25572-miller1-2016.03.13-15.07.46-22102
#
# GAUGEWAVE::betaExact1 (gaugewave-shift_exact)
# 1:iteration 2:time 3:data
# data columns: 3:betaExact1 4:betaExact2 5:betaExact3
0 0 0 0 0
1600 0.25 0 0 0
3200 0.5 0 0 0
4800 0.75 0 0 0
6400 1 0 0 0
8000 1.25 0 0 0
9600 1.5 0 0 0
11200 1.75 0 0 0
12800 2 0 0 0
14400 2.25 0 0 0
16000 2.5 0 0 0
17600 2.75 0 0 0
19200 3 0 0 0
20800 3.25 0 0 0
22400 3.5 0 0 0
24000 3.75 0 0 0
25600 4 0 0 0
27200 4.25 0 0 0
28800 4.5 0 0 0
30400 4.75 0 0 0
32000 5 0 0 0
