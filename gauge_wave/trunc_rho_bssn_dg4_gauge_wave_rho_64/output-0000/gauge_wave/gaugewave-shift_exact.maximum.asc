# Scalar ASCII output created by CarpetIOScalar
# created on nid25132 by miller1 on Mar 13 2016 at 10:07:40-0500
# parameter filename: "/scratch/sciteam/miller1/simulations/trunc_rho_bssn_dg4_gauge_wave_rho_64/output-0000/gauge_wave.par"
# Build ID: build-dgfe2-h2ologin1.ncsa.illinois.edu-miller1-2016.02.08-01.00.23-31758
# Simulation ID: run-gauge_wave-nid25132-miller1-2016.03.13-15.07.36-29518
# Run ID: run-gauge_wave-nid25132-miller1-2016.03.13-15.07.36-29518
#
# GAUGEWAVE::betaExact1 (gaugewave-shift_exact)
# 1:iteration 2:time 3:data
# data columns: 3:betaExact1 4:betaExact2 5:betaExact3
0 0 0 0 0
3200 0.25 0 0 0
6400 0.5 0 0 0
9600 0.75 0 0 0
12800 1 0 0 0
16000 1.25 0 0 0
19200 1.5 0 0 0
22400 1.75 0 0 0
25600 2 0 0 0
28800 2.25 0 0 0
32000 2.5 0 0 0
35200 2.75 0 0 0
38400 3 0 0 0
41600 3.25 0 0 0
44800 3.5 0 0 0
48000 3.75 0 0 0
51200 4 0 0 0
54400 4.25 0 0 0
57600 4.5 0 0 0
60800 4.75 0 0 0
