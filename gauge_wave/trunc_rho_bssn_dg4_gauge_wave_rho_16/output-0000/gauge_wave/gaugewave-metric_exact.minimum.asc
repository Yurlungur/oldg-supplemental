# Scalar ASCII output created by CarpetIOScalar
# created on nid25594 by miller1 on Mar 13 2016 at 10:07:38-0500
# parameter filename: "/scratch/sciteam/miller1/simulations/trunc_rho_bssn_dg4_gauge_wave_rho_16/output-0000/gauge_wave.par"
# Build ID: build-dgfe2-h2ologin1.ncsa.illinois.edu-miller1-2016.02.08-01.00.23-31758
# Simulation ID: run-gauge_wave-nid25594-miller1-2016.03.13-15.07.36-404
# Run ID: run-gauge_wave-nid25594-miller1-2016.03.13-15.07.36-404
#
# GAUGEWAVE::gExact11 (gaugewave-metric_exact)
# 1:iteration 2:time 3:data
# data columns: 3:gExact11 4:gExact12 5:gExact13 6:gExact22 7:gExact23 8:gExact33
0 0 0.99 0 0 1 0 1
800 0.25 0.99 0 0 1 0 1
1600 0.5 0.99 0 0 1 0 1
2400 0.75 0.99 0 0 1 0 1
3200 1 0.99 0 0 1 0 1
4000 1.25 0.99 0 0 1 0 1
4800 1.5 0.99 0 0 1 0 1
5600 1.75 0.99 0 0 1 0 1
6400 2 0.99 0 0 1 0 1
7200 2.25 0.99 0 0 1 0 1
8000 2.5 0.99 0 0 1 0 1
8800 2.75 0.99 0 0 1 0 1
9600 3 0.99 0 0 1 0 1
10400 3.25 0.99 0 0 1 0 1
11200 3.5 0.99 0 0 1 0 1
12000 3.75 0.99 0 0 1 0 1
12800 4 0.99 0 0 1 0 1
13600 4.25 0.99 0 0 1 0 1
14400 4.5 0.99 0 0 1 0 1
15200 4.75 0.99 0 0 1 0 1
16000 5 0.99 0 0 1 0 1
