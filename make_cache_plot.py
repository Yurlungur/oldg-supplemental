#!/usr/bin/env python2
# Author: Jonah Miller (jonah.maxwell.miller@gmail.com)
# Time-stamp: <2016-03-29 14:15:13 (jmiller)>

import numpy as np
import matplotlib as mpl
from matplotlib import pyplot as plt

LINEWIDTH=3
FONTSIZE=16

P_MAX=6.25
P_MIN=1
YMIN=0
YMAX=3
YLIM=2

VFD=48
VDG=92

C = 1500000 # cache size in bytes
p = np.linspace(P_MIN,P_MAX,100)

def CO(p,V):
    """
    The cache overhead
    """
    Ntot = C / (8*V)
    overhead = (Ntot/(Ntot**(1./3)-p)**3) - 1
    return overhead

plt.plot(p,CO(p,VFD),'b-',lw=LINEWIDTH)
plt.plot(p,CO(2,VDG)*np.ones_like(p),'g--',lw=LINEWIDTH)

# Adds some annotations.
#plt.plot(p,CO(4,VFD)*np.ones_like(p),'k:',lw=LINEWIDTH)

# ax = plt.gca()
# ax.fill_between(p,
#                 YLIM*np.ones_like(p),
#                 YMAX*np.ones_like(p),
#                 facecolor='grey',
#                 alpha=0.5)
                
# plt.text(4.25,CO(4,VFD)-0.15,
#          r'$4^{th}$-order finite differences',
#          fontsize=14)
# 
# plt.text(2.05,YLIM-0.15,
#          r'arbitrary maximum acceptable cost',
#          fontsize=14)

xticks=[1,2,3,4,5,6]
xticknames = [str(x) for x in xticks]
plt.xticks(xticks,xticknames,fontsize=FONTSIZE)
plt.xlabel('stencil order',fontsize=FONTSIZE)
plt.ylabel('memory access overhead for BSSN',fontsize=FONTSIZE)

plt.legend(['finite differences','discontinuous Galerkin'],
           loc = 'upper left')

plt.xlim(P_MIN,P_MAX)
plt.ylim(YMIN,YMAX)

plt.savefig('cache_interior.pdf',
            bbox_inches='tight')

