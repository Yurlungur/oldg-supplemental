all: dgfe-for-bssn.pdf
dgfe-for-bssn.pdf: dgfe-for-bssn.tex header.tex\
		   header.tex\
		   dgfe-for-bssn.bib\
	 	   dgfe-v-fd-scaling.pdf\
		   cells-standalone.pdf\
		   trunc_rho_bssn_dg4_h_refinement.pdf\
		   trunc_rho_bssn_dg_p_refinement.pdf\
		   robust_stability_dg4_with_truncation_gxy_2norm_href.pdf\
		   robust_stability_rho_4_with_truncation_gxy_2norm_pref.pdf\
		   bssn_dg4_gauge_wave_1_plus_log_f08_waveform.pdf\
		   trunc_rho_bssn_dg4_shifted_gauge_wave_1plulog_self-convergence_over_time.pdf\
		   trunc_rho_bssn_dgx_shifted_gauge_wave_1plulog_self-convergence_order.pdf\
		   trunc_rho_bssn_dg8_gauge_wave_3d_norm2_error_of_t.pdf\
		   gauge_wave_3D_slice_lowres.pdf\
		   wavetoy_erru_dg_fd_comparison.pdf\
		   flops_overhead.pdf\
		   cache_interior.pdf\
		   trunc_rho_bssn_dg4_gauge_wave_pointwise_errors.pdf\
		   trunc_rho_bssn_dg4_gauge_wave_l2_errors.pdf\
		   broken-domain.pdf\
		   wavetoy_average_no_average_comparison.pdf\
		   wavetoy_erru_dg_fd_comparison_8th_order.pdf\
		   test-functions.pdf

%.pdf: %.tex
	pdflatex $*
	bibtex $*
	pdflatex $*
	pdflatex $*

dgfe-v-fd-scaling.pdf: make_scaling_plot.py
	python2 $^

test-functions.pdf: test-functions.tex
	pdflatex $^

cells-standalone.pdf: cells-standalone.tex
	pdflatex $^

broken-domain.pdf: broken-domain.tex
	pdflatex $^

trunc_rho_bssn_dg4_h_refinement.pdf: visualization_scripts/wavetoy_convergence.py
	python2 $^

trunc_rho_bssn_dg_p_refinement.pdf: visualization_scripts/wavetoy_convergence.py
	python2 $^

robust_stability_dg4_with_truncation_gxy_2norm_href.pdf: visualization_scripts/make_robust_stability_href.py
	python2 $^

robust_stability_rho_4_with_truncation_gxy_2norm_pref.pdf: visualization_scripts/make_robust_stability_pref.py
	python2 $^

bssn_dg4_gauge_wave_1_plus_log_f08_waveform.pdf: visualization_scripts/shifted_gauge_wave_1_plus_log_convergence.py
	python2 $^

trunc_rho_bssn_dg4_shifted_gauge_wave_1plulog_self-convergence_over_time.pdf: visualization_scripts/shifted_gauge_wave_1_plus_log_convergence.py
	python2 $^

trunc_rho_bssn_dgx_shifted_gauge_wave_1plulog_self-convergence_order.pdf: visualization_scripts/shifted_gauge_wave_1_plus_log_convergence.py
	python2 $^

trunc_rho_bssn_dg8_gauge_wave_3d_norm2_error_of_t.pdf: visualization_scripts/gauge_wave_3D_convergence.py
	python2 $^

gauge_wave_3D_slice_lowres.pdf: visualization_scripts/gauge_wave_3D_sliceplot.py
	python2 $^

wavetoy_erru_dg_fd_comparison.pdf: visualization_scripts/wavetoy_dg_fd_comparison.py
	python2 $^

wavetoy_erru_dg_fd_comparison_8th_order.pdf: visualization_scripts/wavetoy_dg_fd_comparison_8th_order.py
	python2 $^

flops_overhead.pdf: plot_flops_overhead.py
	python2 $^

cache_interior.pdf: make_cache_plot.py
	python2 $^

trunc_rho_bssn_dg4_gauge_wave_pointwise_errors.pdf: visualization_scripts/gauge_wave_convergence.py
	python2 $^

trunc_rho_bssn_dg4_gauge_wave_l2_errors.pdf: visualization_scripts/gauge_wave_convergence.py
	python2 $^

wavetoy_average_no_average_comparison.pdf: visualization_scripts/wavetoy_average_no_average_comparison.py
	python2 $^


clean:
	$(RM) *.aux *.bbl *.blg *.end *.log *.out *.pyc	dgfe-for-bssn.pdf dgfe-v-fd-scaling.pdf	cells-standalone.pdf trunc_rho_bssn_dg4_h_refinement.pdf trunc_rho_bssn_dg_p_refinement.pdf robust_stability_dg2_with_truncation_gxy_2norm_href.pdf robust_stability_dg4_with_truncation_gxy_2norm_href.pdf robust_stability_rho_4_with_truncation_gxy_2norm_pref.pdf bssn_dg4_gauge_wave_1_plus_log_f08_waveform.pdf trunc_rho_bssn_dg4_shifted_gauge_wave_1plulog_self-convergence_over_time.pdf trunc_rho_bssn_dgx_shifted_gauge_wave_1plulog_self-convergence_order.pdf trunc_rho_bssn_dg8_gauge_wave_3d_norm2_error_of_t.pdf gauge_wave_3D_slice_lowres.pdf wavetoy_erru_dg_fd_comparison.pdf flops_overhead.pdf cache_interior.pdf trunc_rho_bssn_dg4_gauge_wave_pointwise_errors.pdf trunc_rho_bssn_dg4_gauge_wave_l2_errors.pdf broken-domain.pdf wavetoy_average_no_average_comparison.pdf wavetoy_erru_dg_fd_comparison_8th_order.pdf test-functions.pdf
.PHONY: all clean
