(*
   analytic-timestepping/globals.m
   Author: Jonah Miller (jonah.maxwell.miller@gmail.com)
   Time-stamp: <2015-10-28 18:54:45 (jmiller)>

   This file defines global variables and functions for the analytic
   timestepping Mathematica program. These variables are used by both
   the discontinuous Galerkin scheme and the finite differences
   scheme.
*)

BeginPackage["Globals`"]

order = 2; (* order of approximation of the derivative *)

(*
   Number of discontinuous Galerkin elements. Must be larger than
   domain of dependance for wave equaiton.
*)
numElements=5*11;

(* Width of a given element *)
elementWidth = order+1;

(*
   Number of finite differences grid points. Useful conversion factor.

   We calcoluate this as the number of grid points per narrow element
   (order+1) plus the number of elements, plus two boundary points.
*)
numGridPoints=elementWidth*numElements + 2;

(* Names for boundary conditions *)
PERIODIC="periodic";
ZERO="zero";
WIDE="wide";
NARROW="narrow";

(* Set the boundary conditions *)
boundaryConditions=PERIODIC;

(* Chronicker Delta *)
delta[i_,j_] = If[i==j,1,0];

(* Element width *)
Clear[h];

EndPackage[]