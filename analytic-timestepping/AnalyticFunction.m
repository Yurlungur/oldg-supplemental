(*
   analytic-timestepping/AnalyticFunction.m
   Author: Jonah Miller (jonah.maxwell.miller@gmail.com)
   Time-stamp: <2016-03-28 17:39:11 (jmiller)>
   
   This file defines the anlytic function that solves the wave
   equation that we use for initial data and that we compare our
   numerical solution to.
   
*)

BeginPackage["AnalyticFunction`"]

(*
   Strategy:

   Any linear combination of a right-travelling wave and a
   left-travelling wave will be a solution to the wave equation
   without boundary conditions. In particular, any polynomial in (x-t)
   or (x+t) will be a solution.

   We use a linear combination of these polynomials to generate an
   arbitrary solution in x.

   In principle, this is an exact solution of the kind of initial data
   that can be represented on a computer.
*)


leftTravelling::usage = "The left-travelling polynomial solution to the wave equation.";
leftTravelling[nmax_]:=Total[Table[a[i]*(x+t)^i,{i,1,nmax}]];

rightTravelling::usage = "The right-travelling polynomial solution to the wave equation.";
rightTravelling[nmax_]:=Total[Table[b[i]*(x-t)^i,{i,1,nmax}]];

analyticU::usage = "The analytic solution for u(x,t)";
analyticU[nmax_]:=a[0] + leftTravelling[nmax]+ rightTravelling[nmax];

analyticRho::usage = "The analytic solution for rho(x,t)";
analyticRho[nmax_] := D[analyticU[nmax],t];

makeInitialData::usage = "Returns {u,rho} at time t=0.";
makeInitialData[nmax_] := Module[{u,rho},
				 u=analyticU[nmax] /. {t->0};
				 rho=analyticRho[nmax] /. {t->0};
				 {u,rho}];

EndPackage[]