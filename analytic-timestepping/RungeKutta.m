(*
   Runge-Kutta.m
   Author: Jonah Miller (jonah.maxwell.miller@gmail.com)
   Time-stamp: <2014-12-17 12:04:46 (jonah)>

   This defines the Runge-Kutta algorithm for the analytic
   timestepping Mathematica program. Pass in a state vector, a time,
   and a right-hand-side function, and you get the system after one
   Runge-Kutta timestep
*)

BeginPackage["RungeKutta`"]
    
(* The CFL Factor *)
dt = \[Alpha] * h; (* factor is arbitrary *)

RK2::usage = "Integrate y via RK2. The state vector is y."
RK2[y_,t_,f_] := Module[{k1,k2,yNew,tNew},
			k1=f[t,y];
			k2=f[t+(2/3)*dt,y+(2/3)*dt*k1];
			yNew = y + dt*((1/4)*k1+(3/4)*k2);
			tNew=t+dt;
			{tNew,yNew}];

RK4::usage = "Integrate y via RK4. The state vector is y.";
RK4[y_,t_,f_] := Module[{k1,k2,k3,k4,yNew,tNew},
			k1=f[t,y];
			k2=f[t+(1/2)*dt,y+(1/2)*k1*dt];
			k3=f[t+(1/2)*dt,y+(1/2)*k2*dt];
			k4=f[t+dt,y+k3*dt];
			yNew = y + (1/6)*dt*(k1+2*k2+2*k3+k4);
			tNew = t+dt;
			{tNew,yNew}];

RK::usage = "Wrapper for all Runge-Kutta integrators.";
RK[N_,y_,t_,f_] := Switch[N,
			  2, RK2[y,t,f],
			  4, RK4[y,t,f],
			  _, Throw["No Runge-Kutta of this form!"]];

timeStep::usage = "One RK2 integration timestep. Returns the grid function y."
timeStep[y_,t_,f_] := Last[RK2[y,t,f]];

EndPackage[]1