
#############################################################
# Thorns
#############################################################
ActiveThorns = "
  ADMBase
  ADMCoupling
  ADMMacros
  Boundary
  Carpet
  CarpetIOASCII
  CarpetIOBasic
  CarpetIOHDF5
  CarpetIOScalar
  CarpetLib
  CarpetReduce
  CartGrid3d
  CoordGauge
  Coordinates
  GenericFD
  Formaline
  hwloc
  IOUtil
  LoopControl
  Minkowski
  MoL
  NanChecker
  Noise
  Periodic
  Slab
  StaticConformal
  SpaceMask
  SphericalSurface
  SymBase
  SystemStatistics
  Time
  TmunuBase
"

#############################################################
# Carpet
#############################################################

Carpet::domain_from_multipatch          = "yes"
Carpet::max_refinement_levels           = 1
Carpet::num_integrator_substeps         = 13

#############################################################
# Grid
#############################################################

Coordinates::coordinate_system          = "Cartesian"

Coordinates::store_jacobian             = no
Coordinates::store_jacobian_derivative  = no
Coordinates::store_inverse_jacobian     = no
Coordinates::store_volume_form          = no

Coordinates::patch_xmin                 = -0.5
Coordinates::patch_ymin                 = -0.5
Coordinates::patch_zmin                 = -0.5

Coordinates::patch_xmax                 = 0.5
Coordinates::patch_ymax                 = 0.5
Coordinates::patch_zmax                 = 0.5

Coordinates::ncells_x                   = 40   # (order+1)*n
Coordinates::ncells_y                   = 40
Coordinates::ncells_z                   = 40

Coordinates::stagger_patch_boundaries   = yes
Coordinates::stagger_outer_boundaries   = yes
Coordinates::shiftout_outer_boundaries  = 0

CartGrid3D::type                        = "multipatch"
Periodic::periodic = "yes"
Coordinates::register_symmetry = "no"


#############################################################
# Time integration
#############################################################

Cactus::terminate                       = "time"
Cactus::cctk_final_time                 = 50

MoL::ODE_Method                         = "RK87"
MoL::MoL_Intermediate_Steps             = 13
MoL::MoL_Num_Scratch_Levels             = 13

Time::dtfac                             = 0.05

MethodOfLines::MoL_NaN_Check          = "yes"

#############################################################
# Initial condition
#############################################################
ADMBase::initial_data = "Minkowski"
ADMBase::initial_lapse = "Minkowski"
ADMBase::initial_shift = "Minkowski"
ADMBase::initial_dtlapse = "Minkowski"
ADMBase::initial_dtshift = "Minkowski"

Noise::apply_id_noise = yes
Noise::Amplitude = 1.5625e-12
Noise::id_vars = "
   ADMBase::metric
   ADMBase::curv
   ADMBase::lapse
   ADMBase::shift
   ADMBase::dtlapse
   ADMBase::dtshift
"

#############################################################
# DGFE_BSSN
#############################################################

ActiveThorns = "
   ML_BSSN_DG4
"


ADMBase::evolution_method               = "ML_BSSN_DG4"
ADMBase::lapse_evolution_method         = "ML_BSSN_DG4"
ADMBase::shift_evolution_method         = "ML_BSSN_DG4"
ADMBase::dtlapse_evolution_method       = "ML_BSSN_DG4"
ADMBase::dtshift_evolution_method       = "ML_BSSN_DG4"


# Gauge conditions

ML_BSSN_DG4::harmonicN           = 1     # 1+log
ML_BSSN_DG4::harmonicF           = 2.0   # 1+log
ML_BSSN_DG4::shiftFormulation    = 0     # 1+log
ML_BSSN_DG4::ShiftGammaCoeff     = 0.75
ML_BSSN_DG4::BetaDriver          = 1.0
ML_BSSN_DG4::advectLapse         = 1
ML_BSSN_DG4::advectShift         = 1
ML_BSSN_DG4::minimumLapse        = 0.0
ML_BSSN_DG4::conformalMethod     = 1
ML_BSSN_DG4::epsDiss             = 0
ML_BSSN_DG4::epsJump             = 0.0

# Extra grid information
ML_BSSN_DG4::tile_size = 5   # order+1

Carpet::ghost_size                      = 1
Carpet::granularity                     = 5 # order+1
Carpet::granularity_boundary            = 1
Carpet::poison_new_timelevels           = no  # does not work with persistent RHS

# Extra MoL
MethodOfLines::init_RHS_zero = no

# Extra initial data
ML_BSSN_DG4::initial_boundary_condition = "Minkowski"   # can't extrapolate
ML_BSSN_DG4::rhs_boundary_condition     = "zero"        # newrad doesn't work

# NaN Checker
NaNChecker::check_every     = 1
NaNChecker::verbose         = "all"
NaNChecker::action_if_found = "terminate"
NaNChecker::check_vars      = "
ML_BSSN_DG4::gt11
"

CarpetIOBasic::outInfo_vars         = "
   Carpet::physical_time_per_hour
   SystemStatistics::maxrss_mb
   SystemStatistics::swap_used_mb
   ADMBase::gxy
   ML_BSSN_DG4::H
"

CarpetIOScalar::outScalar_vars      = "
   ADMBase::alp
   ADMBase::gxy
   ADMBase::lapse
   ADMBase::shift
   ML_BSSN_DG4::H
   grid::coordinates
"

IOASCII::out1D_vars           =" 
   ADMBase::gxy
   ADMBase::lapse
   ADMBase::shift
   ML_BSSN_DG4::gt11
   grid::coordinates
"

IOHDF5::out1D_vars           = "
  ADMBase::gxy
  ADMBase::lapse
  ADMBase::shift
  grid::coordinates
"

IOHDF5::out3D_vars           = "
  ADMBase::metric
  grid::coordinates
"


#############################################################
# Checkpointing and recovery
#############################################################

CarpetIOHDF5::checkpoint                    = no
IO::checkpoint_ID                           = no
IO::recover                                 = "autoprobe"
IO::checkpoint_every_walltime_hours         = 3
IO::out_proc_every                          = 2
IO::checkpoint_keep                         = 1
IO::checkpoint_on_terminate                 = yes
IO::checkpoint_dir                          = "../checkpoints"
IO::recover_dir                             = "../checkpoints"
IO::abort_on_io_errors                      = yes
CarpetIOHDF5::open_one_input_file_at_a_time = yes

#############################################################
# Output
#############################################################

IO::out_dir                   = $parfile
IO::out_fileinfo              = "all"

CarpetIOBasic::outInfo_every        = 50
CarpetIOBasic::outInfo_reductions   = "norm2 maximum minimum"
CarpetIOScalar::outScalar_every     = 50
CarpetIOScalar::one_file_per_group  = yes
CarpetIOScalar::outScalar_reductions = "minimum maximum norm2 norm_inf"
IOASCII::out_precision = 17
IOASCII::out1D_every          = 50
IOASCII::one_file_per_group   = yes
IOHDF5::out1D_every          = 50
IOHDF5::out3D_every          = 50
IOHDF5::one_file_per_group   = yes

# IOHDF5::output_symmetry_points = yes
# IOHDF5::out3D_ghosts           = yes
# IOHDF5::out3D_outer_ghosts     = yes
