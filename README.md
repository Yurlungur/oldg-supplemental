# An Operator-Based Local Discontinuous Galerkin Method Compatible With the BSSN Formulation of Einstein's Equations: Supplemental Material

Authors: Jonah Miller <jmiller@perimeterinstitute.ca> and Erik Schnetter <eschnetter@perimeterinstitute.ca>

This repository contains the supplemental materials for our paper: "An Operator-Based Local Discontinuous Galerkin Method Compatible With the BSSN Formulation of Einstein's Equations."

The supplemental material includes the paper itself, which can be created by typing `make` in a unix environment.

## Structure:
* `analytic-timestepping` contains the tools to calculate the convergence for the wave equation.
* `visualization_scripts` contains data analysis tools, written in python.
* `stencils` contains matrices for OLDG stencils of different orders. Element width has been factored out.
* `par` contains parameter files used for simulations. 
* `run` contains scripts for starting simulations. These scripts contain the free parameters used.
* `thornlists` contains the Einstein Toolkit thornlist used. The actual thorn can be found here: <https://bitbucket.org/Yurlungur/mclachlanoldg>.
* `gauge_wave` contains data for gauge wave simulations.
* `gauge_wave_3D` contains data for 3D gauge wave simulations.
* `robust_stability` contains data for the robust stability test.
* `shifted_gauge_wave_1_plus_log` contains data for our gamma driver gauge wave test.
* `wavetoy` contains data for our wave equation tests.

## Dependencies
* The LaTeX dependencies can be found in `header.tex`
* For python scripts:
    * Python2
    * numpy
    * scipy
    * matplotlib
    * hdf5 and h5py
    * yt