ActiveThorns = "
   Boundary
   Carpet
   CarpetIOASCII
   CarpetIOBasic
   CarpetIOScalar
   CarpetIOHDF5
   CarpetLib
   CarpetReduce
   CartGrid3D
   CoordBase
   Coordinates
   Formaline
   GenericFD
   IOUtil
   ML_Wavetoy_FD4
   MoL
   NaNChecker
   Periodic
   Slab
   SymBase
   Time
"

#############################################################
# Grid
#############################################################

CartGrid3D::type               = "multipatch"
Coordinates::coordinate_system = "Cartesian"

Coordinates::patch_xmin = -0.5 - 0.5/(5*8)
Coordinates::patch_ymin = -0.5 - 0.5/(5*8)
Coordinates::patch_zmin = -0.5 - 0.5/(5*8)
Coordinates::patch_xmax = 0.5 - 0.5/(5*8)
Coordinates::patch_ymax = 0.5 - 0.5/(5*8)
Coordinates::patch_zmax = 0.5 - 0.5/(5*8)

Coordinates::ncells_x = 5*8   # (order+1)*n
Coordinates::ncells_y = 5*1 #*8
Coordinates::ncells_z = 5*1 #*8

Coordinates::patch_boundary_size = 3
Coordinates::outer_boundary_size = 3

Coordinates::stagger_patch_boundaries = yes
Coordinates::stagger_outer_boundaries = yes

Coordinates::register_symmetry = no
Periodic::periodic             = yes

Coordinates::store_jacobian            = no
Coordinates::store_inverse_jacobian    = no
Coordinates::store_jacobian_derivative = no
Coordinates::store_volume_form         = no

#############################################################
# Carpet
#############################################################

Carpet::domain_from_multipatch = yes
Carpet::ghost_size             = 3

Carpet::poison_new_timelevels = no   # does not work with persistent RHS

#############################################################
# Time integration
#############################################################

Cactus::terminate       = "time"
Cactus::cctk_final_time = 1

Time::timestep_method = "given"
Time::timestep        = 0.05/8

MethodOfLines::ODE_method             = "RK87"
MethodOfLines::MoL_Intermediate_Steps = 13
MethodOfLines::MoL_Num_Scratch_Levels = 13

MethodOfLines::init_RHS_zero = no

MethodOfLines::MoL_NaN_Check = yes

#############################################################
# ML_WaveToy
#############################################################

ML_WaveToy_FD4::initial_data = "sinex"
ML_WaveToy_FD4::formulation  = "second_order"
ML_WaveToy_FD4::width        = 1.0

ML_WaveToy_FD4::epsDiss = 0

#############################################################
# Output
#############################################################

IO::out_dir = "$parfile"

IOBasic::outInfo_every      = 1*8
IOBasic::outInfo_reductions = "minimum maximum norm2 norm_inf"
IOBasic::outInfo_vars       = "
   ML_WaveToy_FD4::WT_u
   ML_WaveToy_FD4::WT_energy
   ML_WaveToy_FD4::WT_errenergy
"

IOScalar::one_file_per_group = yes
IOScalar::out_precision       = 17

IOScalar::outScalar_every = 1*8
IOScalar::outScalar_vars  = "
   ML_WaveToy_FD4::WT_u
   ML_WaveToy_FD4::WT_erru
   ML_WaveToy_FD4::WT_rho
   ML_WaveToy_FD4::WT_du
   ML_WaveToy_FD4::WT_urhs
   ML_WaveToy_FD4::WT_rhorhs
   ML_WaveToy_FD4::WT_energy
   ML_WaveToy_FD4::WT_weight
   ML_WaveToy_FD4::WT_errenergy
"

IOASCII::one_file_per_group = yes
IOHDF5::one_file_per_group = yes
IOASCII::out_precision      = 17

IOHDF5::out1D_every = 1*8
IOHDF5::out1D_vars  = "
   grid::coordinates
   CarpetReduce::weight
   ML_WaveToy_FD4::WT_u
   ML_WaveToy_FD4::WT_erru
   ML_WaveToy_FD4::WT_rho
   ML_WaveToy_FD4::WT_du
   ML_WaveToy_FD4::WT_urhs
   ML_WaveToy_FD4::WT_rhorhs
   ML_WaveToy_FD4::WT_energy
   ML_WaveToy_FD4::WT_errenergy
   ML_WaveToy_FD4::WT_weight
"

IOHDF5::out3D_every = 1*8
IOHDF5::out3D_vars  = "
   grid::coordinates
   CarpetReduce::weight
   ML_WaveToy_FD4::WT_u
   ML_WaveToy_FD4::WT_erru
   ML_WaveToy_FD4::WT_rho
   ML_WaveToy_FD4::WT_du
   ML_WaveToy_FD4::WT_urhs
   ML_WaveToy_FD4::WT_rhorhs
   ML_WaveToy_FD4::WT_energy
   ML_WaveToy_FD4::WT_errenergy
   ML_WaveToy_FD4::WT_weight
"
