#!/usr/bin/env python2
# Author: Jonah Miller (jonah.maxwell.miller@gmail.com)
# Time-stamp: <2016-03-31 13:31:16 (jmiller)>

import numpy as np
import matplotlib as mpl
from matplotlib import pyplot as plt

min_order = 2
max_order = 13
linewidth = 3
fontsize = 16

def NFDG1(p):
    "FLOPS per grid point for DG first derivative"
    return 2.*(p+2./(1+p))

def NFDG12(p):
    "FLOPS per grid point for DG first and second derivatives."
    return 12.*NFDG1(p)

def NFFD1(p):
    "FLOPS per grid point for FD first derivative"
    return 2.*p

def NFFD12(p):
    "FLOPS per grid point for the FD first and second derivatives."
    return 6.*(p+1)**2

def OVERHEAD1(p):
    return NFDG1(p)/NFFD1(p)

def OVERHEAD2(p):
    return NFDG12(p)/NFFD12(p)

orders_fine = np.linspace(min_order,max_order,100)
plt.plot(orders_fine,OVERHEAD1(orders_fine),'b-',lw=linewidth)
plt.plot(orders_fine,OVERHEAD2(orders_fine),'g--',lw=linewidth)
plt.plot(orders_fine,np.ones_like(orders_fine),'k:',lw=linewidth)
plt.xlim(min_order,max_order)

ax = plt.gca()
ax.annotate('first derivatives',
            xy=(4.2,1.1),
            xytext=(6,1.175),
            arrowprops=dict(arrowstyle="->",
                            connectionstyle="arc3,rad=0.2"),
            fontsize=12)

ax.annotate('first and second derivatives',
            xy=(4.2,0.7),
            xytext=(6,0.775),
            arrowprops=dict(arrowstyle="->",
                            connectionstyle="arc3,rad=0.2"),
            fontsize=12)

plt.xlabel('stencil order',fontsize=fontsize)
plt.ylabel('ratio of computational cost',fontsize=fontsize)

plt.text(max_order-2,0.95,
         'equal cost',
         fontsize=12)

plt.legend(['NF'+r'${_{DG}}^{(1)}/$'+'NF'+r'${_{FD}}^{(1)}$',
            'NF'+r'${_{DG}}^{(1,2)}/$'+'NF'+r'${_{FD}}^{(1,2)}$'],
           loc='lower left')
plt.savefig('flops_overhead.pdf',
            bbox_inches='tight')
