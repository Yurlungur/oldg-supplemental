#!/usr/bin/env python2
# Author: Author: Jonah Miller (jonah.maxwell.miller@gmail.com)

import numpy as np
import matplotlib as mpl
from matplotlib import pyplot as plt
#mpl.rcParams.update({'font.size' : 22})
mpl.rc('xtick', labelsize=16) 
mpl.rc('ytick', labelsize=16) 
mpl.rcParams.update({'font.size': 18})

show = False

#YLABEL = r'$\frac{N_{ghost\ cells}+N_{interior\ cells}}{N_{interior\ cells}}$'
YLABEL = 'communication overhead'
#YLABEL = 'Overhead'
#BGSTYLE='k:'
BGSTYLE='k:'

num_ghost_cell_names = ['perfect scaling', 'discontinuous Galerkin',
                        '4th-order stencil','8th-order stencil']
num_ghost_cells = [0,2,4,8] #{'fd4' : 4, 'fd8': 8, 'dg' : 2, 'perfect' : 0}
problem_size =1000 # ^3
num_processors=np.array([10**(0.25*i) for i in range(30)])
#num_processors=np.arange(1,10000000,10)
SCALE=1
OFFSET=-1
YMIN=SCALE*(0.5+OFFSET)
YLIMIT=SCALE*(2+OFFSET)
YMAX=SCALE*(2.5+OFFSET)
ylimit_val=np.ones_like(num_processors)*YLIMIT
ymax_val = np.ones_like(num_processors)*YMAX
def get_cost_raw(num_procs,num_ghosts):
    return (num_procs * ((problem_size/(num_procs**(1./3)))
                        + num_ghosts)**3)

def get_cost(num_procs,num_ghosts):
    return SCALE*((get_cost_raw(num_procs,num_ghosts)/get_cost_raw(num_procs,0))
                  +OFFSET)

costs = np.empty((len(num_ghost_cells),len(num_processors)))
for i in range(len(num_ghost_cells)):
    costs[i] = get_cost(num_processors,num_ghost_cells[i])

def plot_scaling(costs):
    lines = [plt.semilogx(num_processors,cost,linewidth=5,label=name) \
             for cost,name in zip(costs,num_ghost_cell_names)]
    lines[0][0].set_linestyle('-')
    lines[1][0].set_linestyle('--')
    lines[2][0].set_linestyle('-.')
    lines[3][0].set_linestyle(':')
    ax = plt.gca()
    ax.fill_between(num_processors,ylimit_val,ymax_val,facecolor='grey',alpha=0.5)
    vertical_line=np.linspace(YMIN,YMAX,10)
    lines += [plt.semilogx(35000*np.ones_like(vertical_line),
                           vertical_line,BGSTYLE,linewidth=2)]
    lines += [plt.semilogx(270000*np.ones_like(vertical_line),
                           vertical_line,BGSTYLE,linewidth=2)]
    lines += [plt.semilogx(2.15E6*np.ones_like(vertical_line),
                           vertical_line,BGSTYLE,linewidth=2)]
    ax.annotate('arbitrary maximum acceptable cost', xy=(1.5,YLIMIT),
                 horizontalalignment='left',verticalalignment='top',
                 fontsize=14)
    ax.annotate(r'$3.5x10^4$',xy=(32500,SCALE*0.05+YMIN),
                horizontalalignment='right',verticalalignment='bottom',
                fontsize=12,color='k')#color='c')
    ax.annotate(r'$2.7x10^5$',xy=(260000,SCALE*0.05+YMIN),
                horizontalalignment='right',verticalalignment='bottom',
                fontsize=12,color='k')#color='r')
    ax.annotate(r'$2.2x10^6$',xy=(2.E6,SCALE*0.05+YMIN),
                horizontalalignment='right',verticalalignment='bottom',
                fontsize=12,color='k')#color='g')
    plt.xlabel('number of domains',fontsize=16)
    plt.ylabel(YLABEL,fontsize=16)#fontsize=32)
    plt.ylim(YMIN,YMAX)
    plt.xlim(min(num_processors),max(num_processors))
    ax.legend(loc='upper center', bbox_to_anchor=(0.5, (-0.15)),
              fancybox=False, shadow=False, ncol=2,
              prop={'size':14})
    plt.savefig('dgfe-v-fd-scaling.pdf',bbox_inches='tight')
    if show==True:
        plt.show()
    return

plot_scaling(costs)


