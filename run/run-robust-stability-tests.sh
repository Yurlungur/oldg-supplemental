#!/bin/bash
# Author: Jonah Miller (jonah.maxwell.miller@gmail.com)
# Time-stamp: <2016-03-18 17:53:37 (jmiller)>

# This script runs simulations for the robust stabilty test to test
# for convergence under h- and p-refinement.

PERIODS=25
FORMULATION=DGFE_BSSN
CONFIG=dgfe2
WALLTIME=312:00:00
PROCS=8
PARFILE="par/robust_stability.rpar"

SUBMIT="simfactory/bin/sim submit"
OPTIONS="--config $CONFIG --walltime=${WALLTIME} --procs=${PROCS} --define PERIODS $PERIODS --define FORMULATION $FORMULATION --parfile=${PARFILE}"

for ORDER in {2,4}; do
    for RHO in {4,8}; do
	NAME=trunc_rho_bssn_dg${ORDER}_robust_stability_rho_${RHO}
	$SUBMIT $NAME --define RHO $RHO --define ORDER $ORDER $OPTIONS
    done
done

ORDER=4
RHO=2
NAME=trunc_rho_bssn_dg${ORDER}_robust_stability_rho_${RHO}
$SUBMIT $NAME --define RHO $RHO --define ORDER $ORDER $OPTIONS

ORDER=2
RHO=16
NAME=trunc_rho_bssn_dg${ORDER}_robust_stability_rho_${RHO}
$SUBMIT $NAME --define RHO $RHO --define ORDER $ORDER $OPTIONS

ORDER=8
RHO=4
NAME=trunc_rho_bssn_dg${ORDER}_robust_stability_rho_${RHO}
$SUBMIT $NAME --define RHO $RHO --define ORDER $ORDER $OPTIONS

