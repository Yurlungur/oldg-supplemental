#!/bin/bash
# Author: Jonah Miller (jonah.maxwell.miller@gmail.com)
# Time-stamp: <2016-03-18 17:29:28 (jmiller)>

# This script runs simulations for the wave toy
# to test for convergence under h- and p-refinement.

TFINAL=1
BASESTEP=0.05
EPSDISS=0
CONFIG=dgfe2
WALLTIME=24:00:00
PROCS=8
TYPE=dg
SUBMIT="simfactory/bin/sim submit"

OPTIONS="--config $CONFIG --walltime=${WALLTIME} --procs=${PROCS} --define TFINAL $TFINAL --define BASESTEP $BASESTEP --define EPSDISS $EPSDISS"

for ORDER in {2,4,6,8}; do
    RHO=8
    NAME=wavetoy_so_${TYPE}${ORDER}_sinex_rho_${RHO}
    $SUBMIT $NAME --parfile=par/wavetoy_so_${TYPE}${ORDER}_sinex.par --define RHO $RHO $OPTIONS
done

for RHO in {16,32,64}; do
    ORDER=4
    NAME=wavetoy_so_${TYPE}${ORDER}_sinex_rho_${RHO}
    $SUBMIT $NAME --parfile=par/wavetoy_so_${TYPE}${ORDER}_sinex.par --define RHO $RHO $OPTIONS
done

TYPE=fd
ORDER=4
RHO=8
NAME=wavetoy_so_${TYPE}${ORDER}_sinex_rho_${RHO}
$SUBMIT $NAME --parfile=par/wavetoy_so_${TYPE}${ORDER}_sinex.par --define RHO $RHO $OPTIONS
