#!/bin/bash
# Author: Jonah Miller (jonah.maxwell.miller@gmail.com)
# Time-stamp: <2016-02-29 11:36:02 (jmiller)>

# This script runs simulations for the gamma driver gauge wave.

PERIODS=4
FORMULATION=DGFE_BSSN
CONFIG=dgfe2
WALLTIME=312:00:00
PARFILE="par/shifted_gauge_wave_1_plus_log.rpar"

SUBMIT="simfactory/bin/sim submit"
OPTIONS="--config $CONFIG --walltime=${WALLTIME} --define PERIODS $PERIODS --define FORMULATION $FORMULATION --parfile=${PARFILE}"

ORDER=4
PROCS[32]=8
PROCS[64]=8
PROCS[96]=192
for RHO in {32,64,96}; do
    NAME=trunc_rho_bssn_dg${ORDER}_shifted_gauge_wave_1plulog_rho_${RHO}
    $SUBMIT $NAME --define RHO $RHO --define ORDER $ORDER --procs=${PROCS[${RHO}]} $OPTIONS
done

ORDER=8
PROCS[3]=64
PROCS[6]=128
PROCS[9]=256
for RHO in {3,6,9}; do
    NAME=trunc_rho_bssn_dg${ORDER}_shifted_gauge_wave_1plulog_rho_${RHO}
    $SUBMIT $NAME --define RHO $RHO --define ORDER $ORDER --procs=${PROCS[${RHO}]} $OPTIONS
done
