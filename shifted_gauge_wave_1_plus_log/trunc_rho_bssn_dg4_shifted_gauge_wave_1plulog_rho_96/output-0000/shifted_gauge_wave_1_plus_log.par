
#############################################################
# Thorns
#############################################################
ActiveThorns = "
  ADMBase
  ADMCoupling
  ADMMacros
  Boundary
  Carpet
  CarpetIOASCII
  CarpetIOBasic
  CarpetIOHDF5
  CarpetIOScalar
  CarpetLib
  CarpetReduce
  CartGrid3d
  CoordBase
  CoordGauge
  Coordinates
  Formaline
  GenericFD
  hwloc
  IOUtil
  LoopControl
  MoL
  NanChecker
  NewRad
  Periodic
  ShiftedGaugeWave
  Slab
  SpaceMask
  SphericalSurface
  StaticConformal
  SymBase
  SystemStatistics
  Time
  TmunuBase
"

#############################################################
# Carpet
#############################################################

Carpet::domain_from_multipatch          = "yes"
Carpet::max_refinement_levels           = 1
Carpet::num_integrator_substeps         = 13
Carpet::output_timer_tree_every         = 1
LoopControl::verbose                    = no

Carpet::poison_new_timelevels  = no # Does not work with persistent RHS

#############################################################
# Grid
#############################################################

Coordinates::coordinate_system          = "Cartesian"

Coordinates::store_jacobian             = no
Coordinates::store_jacobian_derivative  = no
Coordinates::store_inverse_Jacobian     = no
Coordinates::store_volume_form          = no

Coordinates::stagger_patch_boundaries   = yes
Coordinates::stagger_outer_boundaries   = yes

Coordinates::patch_xmin                 = -0.5
Coordinates::patch_ymin                 = -0.00260416666667
Coordinates::patch_zmin                 = -0.00260416666667

Coordinates::patch_xmax                 = 0.5
Coordinates::patch_ymax                 = 0.00260416666667
Coordinates::patch_zmax                 = 0.00260416666667

Coordinates::ncells_x                   = 960   # 2*(order+1)*n
Coordinates::ncells_y                   = 5
Coordinates::ncells_z                   = 5

CartGrid3D::type                        = "multipatch"
Periodic::periodic = "yes"
Coordinates::register_symmetry = "no"


#############################################################
# Time integration
#############################################################

Cactus::terminate                       = "time"
Cactus::cctk_final_time                 = 5.0

MoL::ODE_Method                         = "RK87"
MoL::MoL_Intermediate_Steps             = 13
MoL::MoL_Num_Scratch_Levels             = 13

Time::dtfac                             = 0.05

MethodOfLines::init_RHS_zero          = no
MethodOfLines::MoL_NaN_Check          = "yes"

#############################################################
# Initial data
#############################################################

ADMBase::initial_data                = "ShiftedGaugeWave"
ADMBase::initial_lapse               = "ShiftedGaugeWave"
ADMBase::initial_shift               = "ShiftedGaugeWave"
ADMBase::initial_dtlapse             = "ShiftedGaugeWave"
ADMBase::initial_dtshift             = "ShiftedGaugeWave"

ShiftedGaugeWave::amp                          = 0.01
ShiftedGaugeWave::period                       = 1.0

ShiftedGaugeWave::psi = 0
ShiftedGaugeWave::theta = 0
ShiftedGaugeWave::phi = 0

# Outputs exact data too. Convenient for comparisons.
ShiftedGaugeWave::exact_method = "ShiftedGaugeWave"

#############################################################
# DGFE_BSSN
#############################################################

ActiveThorns = "
   ML_BSSN_DG4
"


ADMBase::evolution_method               = "ML_BSSN_DG4"
ADMBase::lapse_evolution_method         = "ML_BSSN_DG4"
ADMBase::shift_evolution_method         = "ML_BSSN_DG4"
ADMBase::dtlapse_evolution_method       = "ML_BSSN_DG4"
ADMBase::dtshift_evolution_method       = "ML_BSSN_DG4"


# Gauge conditions

ML_BSSN_DG4::harmonicN           = 1     # 1+log
ML_BSSN_DG4::harmonicF           = 2.0   # 1+log
ML_BSSN_DG4::shiftFormulation    = 0     # 1+log
ML_BSSN_DG4::ShiftGammaCoeff     = 0.75  # should be 0.75/<length scale>
ML_BSSN_DG4::BetaDriver          = 1.0
ML_BSSN_DG4::evolveB             = 0     # B = d/dt beta
ML_BSSN_DG4::advectLapse         = 1
ML_BSSN_DG4::advectShift         = 1
ML_BSSN_DG4::minimumLapse        = 0.0
ML_BSSN_DG4::conformalMethod     = 1 # 1 for W, 0 for phi
ML_BSSN_DG4::epsDiss             = 0.0
ML_BSSN_DG4::epsJump             = 0.0


# Extra grid information
Carpet::ghost_size                      = 1
Carpet::granularity                     = 5   # order+1
Carpet::granularity_boundary            = 1

# Extra initial data
ML_BSSN_DG4::initial_boundary_condition = "Minkowski" # can't extrapolate
ML_BSSN_DG4::rhs_boundary_condition = "zero"          # newrad doesn't work

# Set the tile size
ML_BSSN_DG4::tile_size = 5

CarpetIOBasic::outInfo_vars         = "
   Carpet::physical_time_per_hour
   SystemStatistics::maxrss_mb
   SystemStatistics::swap_used_mb
"

CarpetIOScalar::outScalar_vars      = "
   ADMBase::metric
   ADMBase::curv
"

IOASCII::out1D_vars           =" 
   grid::coordinates
   CarpetReduce::weight
   ADMBase::metric
   ADMBase::curv
"

IOHDF5::out1D_vars           = "
   grid::coordinates
   CarpetReduce::weight
   ADMBase::metric
   ADMBase::curv
"

IOHDF5::out3D_vars           = "
   grid::coordinates
   CarpetReduce::weight
   ADMBase::metric
   ADMBase::curv
"


#############################################################
# Checkpointing and recovery
#############################################################

CarpetIOHDF5::checkpoint                    = no
IO::checkpoint_ID                           = no
IO::recover                                 = "autoprobe"
IO::checkpoint_every_walltime_hours         = 3
IO::out_proc_every                          = 2
IO::checkpoint_keep                         = 1
IO::checkpoint_on_terminate                 = yes
IO::checkpoint_dir                          = "../checkpoints"
IO::recover_dir                             = "../checkpoints"
IO::abort_on_io_errors                      = yes
CarpetIOHDF5::open_one_input_file_at_a_time = yes

#############################################################
# Output
#############################################################

IO::out_dir                   = $parfile
IO::out_fileinfo              = "all"

CarpetIOBasic::outInfo_every        = 4800
CarpetIOBasic::outInfo_reductions   = "norm2 maximum minimum"
CarpetIOScalar::outScalar_every     = 4800
CarpetIOScalar::one_file_per_group  = yes
CarpetIOScalar::outScalar_reductions = "minimum maximum norm2 norm_inf"
IOASCII::out_precision = 17
IOASCII::out1D_every          = 4800
IOASCII::one_file_per_group   = yes
IOHDF5::out1D_every          = 4800
IOHDF5::out3D_every          = 4800
IOHDF5::one_file_per_group   = yes
