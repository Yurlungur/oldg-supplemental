[LOG:2016-01-28 15:24:43] self.create(simulationName, parfile)::Creating simulation trunc_rho_bssn_dg4_shifted_gauge_wave_1plulog_rho_32
[LOG:2016-01-28 15:24:43] self.create(simulationName, parfile)::Simulation directory: /xfs1/jmiller/simulations/trunc_rho_bssn_dg4_shifted_gauge_wave_1plulog_rho_32
[LOG:2016-01-28 15:24:54] self.create(simulationName, parfile)::Simulation Properties:
[LOG:2016-01-28 15:24:54] self.create(simulationName, parfile)::
[LOG:2016-01-28 15:24:54] self.create(simulationName, parfile)::[properties]
[LOG:2016-01-28 15:24:54] self.create(simulationName, parfile)::machine         = compute
[LOG:2016-01-28 15:24:54] self.create(simulationName, parfile)::simulationid    = simulation-trunc_rho_bssn_dg4_shifted_gauge_wave_1plulog_rho_32-compute-compute.pi.local-jmiller-2016.01.28-15.24.43-31235
[LOG:2016-01-28 15:24:54] self.create(simulationName, parfile)::sourcedir       = /xfs1/jmiller/compute/Cactus
[LOG:2016-01-28 15:24:54] self.create(simulationName, parfile)::configuration   = dgfe2
[LOG:2016-01-28 15:24:54] self.create(simulationName, parfile)::configid        = config-dgfe2-compute-xfs1-jmiller-compute-Cactus
[LOG:2016-01-28 15:24:54] self.create(simulationName, parfile)::buildid         = build-dgfe2-compute-jmiller-2015.12.16-04.26.30-10022
[LOG:2016-01-28 15:24:54] self.create(simulationName, parfile)::testsuite       = False
[LOG:2016-01-28 15:24:54] self.create(simulationName, parfile)::executable      = /xfs1/jmiller/simulations/trunc_rho_bssn_dg4_shifted_gauge_wave_1plulog_rho_32/SIMFACTORY/exe/cactus_dgfe2
[LOG:2016-01-28 15:24:54] self.create(simulationName, parfile)::optionlist      = /xfs1/jmiller/simulations/trunc_rho_bssn_dg4_shifted_gauge_wave_1plulog_rho_32/SIMFACTORY/cfg/OptionList
[LOG:2016-01-28 15:24:54] self.create(simulationName, parfile)::submitscript    = /xfs1/jmiller/simulations/trunc_rho_bssn_dg4_shifted_gauge_wave_1plulog_rho_32/SIMFACTORY/run/SubmitScript
[LOG:2016-01-28 15:24:54] self.create(simulationName, parfile)::runscript       = /xfs1/jmiller/simulations/trunc_rho_bssn_dg4_shifted_gauge_wave_1plulog_rho_32/SIMFACTORY/run/RunScript
[LOG:2016-01-28 15:24:54] self.create(simulationName, parfile)::parfile         = /xfs1/jmiller/simulations/trunc_rho_bssn_dg4_shifted_gauge_wave_1plulog_rho_32/SIMFACTORY/par/shifted_gauge_wave_1_plus_log.rpar
[LOG:2016-01-28 15:24:54] self.create(simulationName, parfile)::
[LOG:2016-01-28 15:24:54] self.create(simulationName, parfile)::Simulation trunc_rho_bssn_dg4_shifted_gauge_wave_1plulog_rho_32 created
[LOG:2016-01-28 15:24:55] self.submit(submitScript)::Restart for simulation trunc_rho_bssn_dg4_shifted_gauge_wave_1plulog_rho_32 created with restart id 0, long restart id 0000
[LOG:2016-01-28 15:24:55] self.submit(submitScript)::Prepping for submission
[LOG:2016-01-28 15:24:55] self.submit(submitScript)::No previous walltime available to be used, using walltime 312:00:00
[LOG:2016-01-28 15:24:55] self.submit(submitScript)::Defined substituion properties for submission
[LOG:2016-01-28 15:24:55] self.submit(submitScript)::{'SIMULATION_ID': 'simulation-trunc_rho_bssn_dg4_shifted_gauge_wave_1plulog_rho_32-compute-compute.pi.local-jmiller-2016.01.28-15.24.43-31235', 'NODE_PROCS': 2, 'PPN_USED': 8, 'DIAGONAL': '0', 'PPN': 8, 'ALLOCATION': '', 'WALLTIME_HH': '312', 'CPUFREQ': '2.4', 'USER': 'jmiller', 'RUNDIR': '/xfs1/jmiller/simulations/trunc_rho_bssn_dg4_shifted_gauge_wave_1plulog_rho_32/output-0000', 'ORDER': '4', 'NODES': 1, 'SIMULATION_NAME': 'trunc_rho_bssn_dg4_shifted_gauge_wave_1plulog_rho_32', 'WALLTIME': '312:00:00', 'NUM_THREADS': 4, 'EXECUTABLE': '/xfs1/jmiller/simulations/trunc_rho_bssn_dg4_shifted_gauge_wave_1plulog_rho_32/SIMFACTORY/exe/cactus_dgfe2', 'PROCS_REQUESTED': 8, 'EMAIL': 'jmiller', 'RESTART_ID': 0, 'CHAINED_JOB_ID': '', 'FROM_RESTART_COMMAND': '', 'NUM_SMT': 1, 'FORMULATION': 'DGFE_BSSN', 'WALLTIME_SECONDS': 1123200, 'SIMFACTORY': '/xfs1/jmiller/compute/Cactus/repos/simfactory2/bin/sim', 'PROCS': 8, 'SUBMITSCRIPT': '/xfs1/jmiller/simulations/trunc_rho_bssn_dg4_shifted_gauge_wave_1plulog_rho_32/output-0000/SIMFACTORY/SubmitScript', 'WALLTIME_HOURS': 312.0, 'PERIODS': '5', 'WALLTIME_MM': '00', 'PARFILE': '/xfs1/jmiller/simulations/trunc_rho_bssn_dg4_shifted_gauge_wave_1plulog_rho_32/output-0000/shifted_gauge_wave_1_plus_log.rpar', 'WALLTIME_SS': '00', 'QUEUE': 'NOQUEUE', 'CONFIGURATION': 'dgfe2', 'SOURCEDIR': '/xfs1/jmiller/compute/Cactus', 'HOSTNAME': 'compute.pi.local', 'NUM_PROCS': 2, 'SCRIPTFILE': '/xfs1/jmiller/simulations/trunc_rho_bssn_dg4_shifted_gauge_wave_1plulog_rho_32/output-0000/SIMFACTORY/SubmitScript', 'RHO': '32', 'MEMORY': '40960', 'WALLTIME_MINUTES': 18720, 'SHORT_SIMULATION_NAME': 'trunc_rho_bssn_'}
[LOG:2016-01-28 15:24:55] self.submit(submitScript)::self.Properties: /xfs1/jmiller/simulations/trunc_rho_bssn_dg4_shifted_gauge_wave_1plulog_rho_32/output-0000/SIMFACTORY/properties.ini
[LOG:2016-01-28 15:24:55] self.submit(submitScript)::
[LOG:2016-01-28 15:24:55] self.submit(submitScript)::[properties]
[LOG:2016-01-28 15:24:55] self.submit(submitScript)::machine         = compute
[LOG:2016-01-28 15:24:55] self.submit(submitScript)::simulationid    = simulation-trunc_rho_bssn_dg4_shifted_gauge_wave_1plulog_rho_32-compute-compute.pi.local-jmiller-2016.01.28-15.24.43-31235
[LOG:2016-01-28 15:24:55] self.submit(submitScript)::sourcedir       = /xfs1/jmiller/compute/Cactus
[LOG:2016-01-28 15:24:55] self.submit(submitScript)::configuration   = dgfe2
[LOG:2016-01-28 15:24:55] self.submit(submitScript)::configid        = config-dgfe2-compute-xfs1-jmiller-compute-Cactus
[LOG:2016-01-28 15:24:55] self.submit(submitScript)::buildid         = build-dgfe2-compute-jmiller-2015.12.16-04.26.30-10022
[LOG:2016-01-28 15:24:55] self.submit(submitScript)::testsuite       = False
[LOG:2016-01-28 15:24:55] self.submit(submitScript)::executable      = /xfs1/jmiller/simulations/trunc_rho_bssn_dg4_shifted_gauge_wave_1plulog_rho_32/SIMFACTORY/exe/cactus_dgfe2
[LOG:2016-01-28 15:24:55] self.submit(submitScript)::optionlist      = /xfs1/jmiller/simulations/trunc_rho_bssn_dg4_shifted_gauge_wave_1plulog_rho_32/SIMFACTORY/cfg/OptionList
[LOG:2016-01-28 15:24:55] self.submit(submitScript)::submitscript    = /xfs1/jmiller/simulations/trunc_rho_bssn_dg4_shifted_gauge_wave_1plulog_rho_32/SIMFACTORY/run/SubmitScript
[LOG:2016-01-28 15:24:55] self.submit(submitScript)::runscript       = /xfs1/jmiller/simulations/trunc_rho_bssn_dg4_shifted_gauge_wave_1plulog_rho_32/SIMFACTORY/run/RunScript
[LOG:2016-01-28 15:24:55] self.submit(submitScript)::parfile         = /xfs1/jmiller/simulations/trunc_rho_bssn_dg4_shifted_gauge_wave_1plulog_rho_32/SIMFACTORY/par/shifted_gauge_wave_1_plus_log.rpar
[LOG:2016-01-28 15:24:55] self.submit(submitScript)::chainedjobid    = -1
[LOG:2016-01-28 15:24:55] self.submit(submitScript)::ppn             = 8
[LOG:2016-01-28 15:24:55] self.submit(submitScript)::procsrequested  = 8
[LOG:2016-01-28 15:24:55] self.submit(submitScript)::allocation      = 
[LOG:2016-01-28 15:24:55] self.submit(submitScript)::user            = jmiller
[LOG:2016-01-28 15:24:55] self.submit(submitScript)::numsmt          = 1
[LOG:2016-01-28 15:24:55] self.submit(submitScript)::walltime        = 312:00:00
[LOG:2016-01-28 15:24:55] self.submit(submitScript)::numprocs        = 2
[LOG:2016-01-28 15:24:55] self.submit(submitScript)::nodeprocs       = 2
[LOG:2016-01-28 15:24:55] self.submit(submitScript)::numthreads      = 4
[LOG:2016-01-28 15:24:55] self.submit(submitScript)::hostname        = compute.pi.local
[LOG:2016-01-28 15:24:55] self.submit(submitScript)::ppnused         = 8
[LOG:2016-01-28 15:24:55] self.submit(submitScript)::queue           = NOQUEUE
[LOG:2016-01-28 15:24:55] self.submit(submitScript)::cpufreq         = 2.4
[LOG:2016-01-28 15:24:55] self.submit(submitScript)::procs           = 8
[LOG:2016-01-28 15:24:55] self.submit(submitScript)::memory          = 40960
[LOG:2016-01-28 15:24:55] self.submit(submitScript)::nodes           = 1
[LOG:2016-01-28 15:24:55] self.submit(submitScript)::pbsSimulationName= trunc_rho_bssn_
[LOG:2016-01-28 15:24:55] self.submit(submitScript)::
[LOG:2016-01-28 15:24:55] self.submit(submitScript)::saving substituted submitscript contents to: /xfs1/jmiller/simulations/trunc_rho_bssn_dg4_shifted_gauge_wave_1plulog_rho_32/output-0000/SIMFACTORY/SubmitScript
[LOG:2016-01-28 15:24:55] self.submit(submitScript)::Executing submission command: exec /xfs1/jmiller/simulations/trunc_rho_bssn_dg4_shifted_gauge_wave_1plulog_rho_32/output-0000/SIMFACTORY/SubmitScript < /dev/null > /dev/null 2> /dev/null & echo $!
[LOG:2016-01-28 15:24:55] self.makeActive()::Simulation trunc_rho_bssn_dg4_shifted_gauge_wave_1plulog_rho_32 with restart-id 0 has been made active
[LOG:2016-01-28 15:24:55] job_id = self.extractJobId(output)::received raw output: 31341
[LOG:2016-01-28 15:24:55] job_id = self.extractJobId(output)::
[LOG:2016-01-28 15:24:55] job_id = self.extractJobId(output)::using submitRegex: (.*)
[LOG:2016-01-28 15:24:55] self.submit(submitScript)::After searching raw output, it was determined that the job_id is: 31341
[LOG:2016-01-28 15:24:55] self.submit(submitScript)::Simulation trunc_rho_bssn_dg4_shifted_gauge_wave_1plulog_rho_32, with restart id 0, and job id 31341 has been submitted
[LOG:2016-01-28 15:24:56] self.load(simulationName, restartId)::For simulation trunc_rho_bssn_dg4_shifted_gauge_wave_1plulog_rho_32, loaded restart id 0, long restart id 0000
[LOG:2016-01-28 15:24:56] self.run()::Prepping for execution/run
[LOG:2016-01-28 15:24:56] checkpointing = self.PrepareCheckpointing(recover_id)::PrepareCheckpointing: max_restart_id: -1
[LOG:2016-01-28 15:24:56] self.run()::Defined substitution properties for execution/run
[LOG:2016-01-28 15:24:56] self.run()::{'SIMULATION_ID': 'simulation-trunc_rho_bssn_dg4_shifted_gauge_wave_1plulog_rho_32-compute-compute.pi.local-jmiller-2016.01.28-15.24.43-31235', 'NODE_PROCS': '2', 'PPN_USED': '8', 'PPN': '8', 'WALLTIME_HH': '312', 'CPUFREQ': '2.4', 'USER': 'jmiller', 'RUNDIR': '/xfs1/jmiller/simulations/trunc_rho_bssn_dg4_shifted_gauge_wave_1plulog_rho_32/output-0000', 'NODES': '1', 'SIMULATION_NAME': 'trunc_rho_bssn_dg4_shifted_gauge_wave_1plulog_rho_32', 'WALLTIME': '312:00:00', 'NUM_THREADS': '4', 'EXECUTABLE': '/xfs1/jmiller/simulations/trunc_rho_bssn_dg4_shifted_gauge_wave_1plulog_rho_32/SIMFACTORY/exe/cactus_dgfe2', 'PROCS_REQUESTED': '8', 'RESTART_ID': 0, 'NUM_SMT': '1', 'WALLTIME_SECONDS': 1123200, 'CONFIGURATION': 'dgfe2', 'PROCS': '8', 'SUBMITSCRIPT': '/xfs1/jmiller/simulations/trunc_rho_bssn_dg4_shifted_gauge_wave_1plulog_rho_32/SIMFACTORY/run/SubmitScript', 'WALLTIME_MM': '00', 'MACHINE': 'compute', 'PARFILE': '/xfs1/jmiller/simulations/trunc_rho_bssn_dg4_shifted_gauge_wave_1plulog_rho_32/output-0000/shifted_gauge_wave_1_plus_log.par', 'WALLTIME_SS': '00', 'WALLTIME_HOURS': 312.0, 'SOURCEDIR': '/xfs1/jmiller/compute/Cactus', 'HOSTNAME': 'compute.pi.local', 'RUNDEBUG': 0, 'NUM_PROCS': '2', 'SCRIPTFILE': '/xfs1/jmiller/simulations/trunc_rho_bssn_dg4_shifted_gauge_wave_1plulog_rho_32/SIMFACTORY/run/SubmitScript', 'MEMORY': '40960', 'WALLTIME_MINUTES': 18720, 'SHORT_SIMULATION_NAME': 'trunc_rho_bssn_'}
[LOG:2016-01-28 15:24:56] self.run()::Executing run command: /xfs1/jmiller/simulations/trunc_rho_bssn_dg4_shifted_gauge_wave_1plulog_rho_32/output-0000/SIMFACTORY/RunScript
