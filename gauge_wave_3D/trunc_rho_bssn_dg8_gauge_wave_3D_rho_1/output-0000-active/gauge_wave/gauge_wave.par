
#############################################################
# Thorns
#############################################################
ActiveThorns = "
  ADMBase
  ADMCoupling
  ADMMacros
  Boundary
  Carpet
  CarpetIOASCII
  CarpetIOBasic
  CarpetIOHDF5
  CarpetIOScalar
  CarpetLib
  CarpetReduce
  CartGrid3d
  CoordBase
  CoordGauge
  Coordinates
  Formaline
  GaugeWave
  GenericFD
  hwloc
  IOUtil
  LoopControl
  MoL
  NanChecker
  NewRad
  Periodic
  Slab
  SpaceMask
  SphericalSurface
  StaticConformal
  SymBase
  SystemStatistics
  Time
  TmunuBase
"

#############################################################
# Carpet
#############################################################

Carpet::domain_from_multipatch          = "yes"
Carpet::max_refinement_levels           = 1
Carpet::num_integrator_substeps         = 13
Carpet::output_timer_tree_every         = 1
LoopControl::verbose                    = no

Carpet::poison_new_timelevels  = no # Does not work with persistent RHS

#############################################################
# Grid
#############################################################

Coordinates::coordinate_system          = "Cartesian"

Coordinates::store_jacobian             = no
Coordinates::store_jacobian_derivative  = no
Coordinates::store_inverse_Jacobian     = no
Coordinates::store_volume_form          = no

Coordinates::stagger_patch_boundaries   = yes
Coordinates::stagger_outer_boundaries   = yes

Coordinates::patch_xmin                 = -0.5
Coordinates::patch_ymin                 = -0.5
Coordinates::patch_zmin                 = -0.5

Coordinates::patch_xmax                 = 0.5
Coordinates::patch_ymax                 = 0.5
Coordinates::patch_zmax                 = 0.5

Coordinates::ncells_x                   = 18   # 2*(order+1)*n
Coordinates::ncells_y                   = 18
Coordinates::ncells_z                   = 18

CartGrid3D::type                        = "multipatch"
Periodic::periodic = "yes"
Coordinates::register_symmetry = "no"


#############################################################
# Time integration
#############################################################

Cactus::terminate                       = "time"
Cactus::cctk_final_time                 = 5.0

MoL::ODE_Method                         = "RK87"
MoL::MoL_Intermediate_Steps             = 13
MoL::MoL_Num_Scratch_Levels             = 13

Time::dtfac                             = 0.0264705882353

MethodOfLines::init_RHS_zero          = "no"
MethodOfLines::MoL_NaN_Check          = "yes"

#############################################################
# Initial data
#############################################################

ADMBase::initial_data                = "GaugeWave"
ADMBase::initial_lapse               = "GaugeWave"
ADMBase::initial_shift               = "GaugeWave"
ADMBase::initial_dtlapse             = "GaugeWave"
ADMBase::initial_dtshift             = "GaugeWave"

GaugeWave::amp                          = 0.01
GaugeWave::period                       = 0.57735026919

GaugeWave::psi = -1.92167573767
GaugeWave::theta = 0.662145235646
GaugeWave::phi = 1.21991691592

# Outputs exact data too. Convenient for comparisons.
GaugeWave::exact_method = "GaugeWave"

#############################################################
# DGFE_BSSN
#############################################################

ActiveThorns = "
   ML_BSSN_DG8
"


ADMBase::evolution_method               = "ML_BSSN_DG8"
ADMBase::lapse_evolution_method         = "ML_BSSN_DG8"
ADMBase::shift_evolution_method         = "ML_BSSN_DG8"
ADMBase::dtlapse_evolution_method       = "ML_BSSN_DG8"
ADMBase::dtshift_evolution_method       = "ML_BSSN_DG8"


# Gauge conditions

ML_BSSN_DG8::harmonicN           = 2     # harmonic
ML_BSSN_DG8::harmonicF           = 1.0   # harmonic
ML_BSSN_DG8::shiftFormulation    = 0     # gamma driver
ML_BSSN_DG8::evolveB             = 0     # Do not evolve the shift (harmonic)
ML_BSSN_DG8::shiftGammaCoeff     = 0     # Do not evolve the shift (gamma driver)
ML_BSSN_DG8::BetaDriver          = 1
ML_BSSN_DG8::advectLapse         = 1
ML_BSSN_DG8::advectShift         = 1
ML_BSSN_DG8::minimumLapse        = 1e-8
ML_BSSN_DG8::conformalMethod     = 0     # 1 for W, 0 for phi
ML_BSSN_DG8::epsDiss             = 0
ML_BSSN_DG8::epsJump             = 0.0

# Extra grid information
Carpet::ghost_size                      = 1
Carpet::granularity                     = 9   # order+1
Carpet::granularity_boundary            = 1

# Extra initial data
ML_BSSN_DG8::initial_boundary_condition = "Minkowski" # can't extrapolate
ML_BSSN_DG8::rhs_boundary_condition = "zero"          # newrad doesn't work

# Set the tile size
ML_BSSN_DG8::tile_size = 9

CarpetIOBasic::outInfo_vars         = "
   Carpet::physical_time_per_hour
   SystemStatistics::maxrss_mb
   SystemStatistics::swap_used_mb
"

CarpetIOScalar::outScalar_vars      = "
   ADMBase::metric
   ADMBase::curv
   ADMBase::lapse
   ADMBase::shift
   ADMBase::dtshift
   GaugeWave::metric_exact
   GaugeWave::curv_exact
   GaugeWave::lapse_exact
   GaugeWave::shift_exact
"

IOASCII::out1D_vars           =" 
   grid::coordinates
   CarpetReduce::weight
   ADMBase::metric
   ADMBase::curv
   ADMBase::lapse
   ADMBase::shift
   ADMBase::dtshift
   GaugeWave::metric_exact
   GaugeWave::curv_exact
   GaugeWave::lapse_exact
   GaugeWave::shift_exact
"

IOHDF5::out1D_vars           = "
   grid::coordinates
   CarpetReduce::weight
   ADMBase::metric
   ADMBase::curv
   GaugeWave::metric_exact
   GaugeWave::curv_exact
"

IOHDF5::out3D_vars           = "
   grid::coordinates
   CarpetReduce::weight
   ADMBase::metric
   ADMBase::curv
   GaugeWave::metric_exact
   GaugeWave::curv_exact
"


#############################################################
# Checkpointing and recovery
#############################################################

CarpetIOHDF5::checkpoint                    = no
IO::checkpoint_ID                           = no
IO::recover                                 = "autoprobe"
IO::checkpoint_every_walltime_hours         = 3
IO::out_proc_every                          = 2
IO::checkpoint_keep                         = 1
IO::checkpoint_on_terminate                 = yes
IO::checkpoint_dir                          = "../checkpoints"
IO::recover_dir                             = "../checkpoints"
IO::abort_on_io_errors                      = yes
CarpetIOHDF5::open_one_input_file_at_a_time = yes

#############################################################
# Output
#############################################################

IO::out_dir                   = $parfile
IO::out_fileinfo              = "all"

CarpetIOBasic::outInfo_every        = 170
CarpetIOBasic::outInfo_reductions   = "maximum minimum norm2"
CarpetIOScalar::outScalar_every     = 170
CarpetIOScalar::one_file_per_group  = yes
CarpetIOScalar::outScalar_reductions = "minimum maximum norm2"
IOASCII::out_precision = 17
IOASCII::out1D_every          = 170
IOASCII::one_file_per_group   = yes
IOHDF5::out1D_every          = 170
IOHDF5::out3D_every          = 170
IOHDF5::one_file_per_group   = yes
